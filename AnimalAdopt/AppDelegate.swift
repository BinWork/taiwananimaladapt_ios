//
//  AppDelegate.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/05.
//

import UIKit
//import Firebase
//import FirebaseAuth

@main
class AppDelegate: UIResponder, UIApplicationDelegate {



    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        if #available(iOS 13.0, *){
        }else{
            guard var window = window else { return true}
            window = UIWindow(frame: UIScreen.main.bounds)
            
            let tabBarController = MainTabBarController()
            window.rootViewController = tabBarController

            // 將 UIWindow 設置為可見的
            window.makeKeyAndVisible()
            
            initFirebase()
        }
        return true
    }


    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        DataStoreManager.favorite.updateLocal()
    }


}


extension AppDelegate{
    
    private func initFirebase(){
//        FirebaseApp.configure()
//        let db = Firestore.firestore()
//        track(db) // silence warning
//        signInAnonymousely()
    }
    
    private func signInAnonymousely(){
//        Auth.auth().signInAnonymously { (authData, error) in
//            if let _error = error{
//                ErrorManager.shared.handle(_error)
//                return
//            }
//            guard let _authData = authData else { return }
//
//        }
    }
    
}
