//
//  AnimalFoundDetailViewController.swift
//  AnimalFound
//
//  Created by nwfmbin2 on 2021/07/13.
//

import UIKit

class AnimalFoundDetailViewController: BaseAnimalDetailViewController {
    
    var viewModel: AnimalFoundDetailViewModel
    
    let stackAnimalDetailed: AnimalDetailedView = {
        let v = AnimalDetailedView()
        v.backgroundColor = .defaultBackgroundColor
        return v
    }()
    
    let stackOwnerDetailed: AnimalDetailedView = {
        let v = AnimalDetailedView()
        v.backgroundColor = .defaultBackgroundColor
        return v
    }()
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imageView.makeBorder(width: .animalImageBorderWidth * 1.5, color: viewModel.gender.color)
        stackAnimalDetailed.makeCorner(radius: 10)
        stackOwnerDetailed.makeCorner(radius: 10)
    }
    
    init(viewModel new: AnimalFoundDetailViewModel) {
        self.viewModel = new
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        addUI()
    }
    
    private func addUI(){
        
        scrollView.addSubview(stackAnimalDetailed)
        stackAnimalDetailed.snp.makeConstraints { (make) in
            make.top.equalTo(viewFeature.snp.bottom).offset(snpOffset.top * 2)
            make.left.equalTo(view).offset(snpOffset.left * 2)
            make.right.equalTo(view).offset(snpOffset.right * 2)
        }
        
        scrollView.addSubview(stackOwnerDetailed)
        stackOwnerDetailed.snp.makeConstraints { (make) in
            make.top.equalTo(stackAnimalDetailed.snp.bottom).offset(snpOffset.top * 2)
            make.left.equalTo(view).offset(snpOffset.left * 2)
            make.right.equalTo(view).offset(snpOffset.right * 2)
            make.bottom.equalToSuperview().offset(snpOffset.bottom * 2)
        }
        
    }
}

extension AnimalFoundDetailViewController{
    
    
    private func setup(){
        
        title = viewModel.petName.represent
        
        imageView.viewModel = viewModel.imageViewModel
        
        lblDate.text = nil
        
        stackAnimalDetailed.viewModel.animalViewModel = viewModel
        stackAnimalDetailed.setup(sections: [.chip, .feature, .kind, .lostPlace, .lostDate])
        
        stackOwnerDetailed.viewModel.animalViewModel = viewModel
        stackOwnerDetailed.setup(sections: [.ownerName, .ownerTel, .ownerEmail])
    }
    
}
