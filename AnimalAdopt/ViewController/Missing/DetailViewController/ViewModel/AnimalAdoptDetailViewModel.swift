//
//  AnimalAdoptDetailViewModel.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/12.
//

import Foundation
import UIKit.UIImageView

class AnimalAdoptDetailViewModel{
    
    var id: String
    var albumFile: String
    var createDate: String
    var coat: String
    var foundPlace: String
    var remark: String
    var shelterName: String
    var shelterAddress: String
    var openDate: String
    var shelterTel: String
    var lastUpdate: String
    
    var imageViewModel: ImageViewModel
    var kind: Animal.Kind
    var gender: Animal.Gender
    var statusAvailable: Animal.Status
    var sterilization: Animal.Boolean
    var bacterin: Animal.Boolean
    var bodyType: Animal.BodyType
    var age: Animal.Age
    
    init(viewModel: AnimalAdoptTableViewCellViewModel) {
        
        self.id = viewModel.id.toString
        self.albumFile = viewModel.albumFile
        self.createDate = viewModel.createTime
        self.coat = viewModel.color
        self.foundPlace = viewModel.foundPlace
        self.remark = viewModel.remark
        self.shelterName = viewModel.shelterName ?? ""
        self.shelterAddress = viewModel.shelterAddress ?? ""
        self.openDate = viewModel.openDate
        self.shelterTel = viewModel.shelterTel ?? ""
        self.lastUpdate = viewModel.lastUpdate
        
        self.imageViewModel = viewModel.imageViewModel
        self.kind = viewModel.kind
        self.gender = viewModel.gender
        self.statusAvailable = viewModel.status
        self.sterilization = viewModel.sterilization
        self.bacterin = viewModel.bacterin
        self.bodyType = viewModel.bodyType
        self.age = viewModel.age
        
    }
    
    
}
