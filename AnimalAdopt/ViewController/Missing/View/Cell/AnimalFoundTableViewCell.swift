//
//  AnimalFoundTableViewCell.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/13.
//

import UIKit

class AnimalFoundTableViewCell: UITableViewCell {
    
    static var cellID: String = String(describing: self)

    private var viewModel: AnimalFoundTableViewCellViewModel?
    
    
    
    private var imageAnimal: AnimalPictureView = {
        let v = AnimalPictureView()
        v.contentMode = .scaleAspectFill
        v.clipsToBounds = true
        v.image = nil
        return v
    }()
    
    private let viewFeature: AnimalFeatureView = {
        let v = AnimalFeatureView()
        return v
    }()
    
    private var stackPlaceFound: UIStackView = {
        let s = UIStackView()
        s.set(Axis: .horizontal, Spacing: 5, Distribution: .fillProportionally)
        return s
    }()
    
    private let imageViewNet: UIImageView = {
        let v = UIImageView()
        v.contentMode = .scaleAspectFit
        return v
    }()
    
    private let lblPlaceLost: UILabel = {
        let lbl = UILabel()
        lbl.lineBreakMode = .byTruncatingTail
        return lbl
    }()
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateUIOnLayoutSubview()
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .defaultBackgroundColor
        
        addUI()
        setStackFoundPlace()
    }
    
    private func addUI(){
        
        contentView.addSubview(imageAnimal)
        imageAnimal.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(snpOffset.left * 2)
            make.height.equalToSuperview().multipliedBy(0.64)
            make.width.equalTo(imageAnimal.snp.height)
            
        }
        
        contentView.addSubview(viewFeature)
        viewFeature.snp.makeConstraints { (make) in
            make.bottom.equalTo(snp.centerY).offset(snpOffset.bottom * 2)
            make.left.equalTo(imageAnimal.snp.right).offset(snpOffset.left * 2)
            make.right.lessThanOrEqualToSuperview().offset(snpOffset.right * 2)
            make.height.equalTo(20)
        }
        contentView.addSubview(stackPlaceFound)
        stackPlaceFound.snp.makeConstraints { (make) in
            make.top.equalTo(snp.centerY).offset(snpOffset.top * 2)
            make.left.equalTo(imageAnimal.snp.right).offset(snpOffset.left * 2)
            make.right.equalToSuperview().offset(snpOffset.right * 3)
            make.height.equalTo(20)
        }
        
    }
    
    
    private func updateUIOnLayoutSubview(){
        imageAnimal.makeCornerRound()
        if let model = viewModel{
            imageAnimal.makeBorder(width: .animalImageBorderWidth, color: model.gender.color)
        }else{
            imageAnimal.makeBorder(width: .animalImageBorderWidth, color: .defaultTextColor)
        }
        makeCorner(radius: .defaultCornerWidth)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension AnimalFoundTableViewCell{
    
    func setup(viewModel new: AnimalFoundTableViewCellViewModel, index: Int){
        
        viewModel = new
        
        guard let _viewModel = viewModel else { return }
        
        viewFeature.viewModel = .init(
            kind: _viewModel.petCategory,
            description: _viewModel.getAnimalDescription(),
            gender: _viewModel.gender
        )
        viewFeature.setup()
        
        imageAnimal.viewModel = _viewModel.imageViewModel
        imageViewNet.image = UIImage(named: ImageName.Animal.missing_location)
        lblPlaceLost.text = _viewModel.lostPlace.represent

    }
    
    
}


extension AnimalFoundTableViewCell{
    
    
    private func setStackFoundPlace(){
        stackPlaceFound.addArrangedSubview(imageViewNet)
        imageViewNet.snp.makeConstraints { (make) in
            make.width.height.equalTo(20)
        }
        stackPlaceFound.addArrangedSubview(lblPlaceLost)
    }
    
    
}
