//
//  AnimalFoundTableViewCellViewModel.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/13.
//

import UIKit.UIApplication

class AnimalFoundTableViewCellViewModel{
    
    var imageViewModel: ImageViewModel
    var chipNum: String
    var petName: String
    var petCategory: Animal.Kind
    var gender: Animal.Gender
    var variety: String
    var coat: String
    var exterior: String
    var feature: String
    var lostTime: String
    var lostPlace: String
    var feederName: String
    var phoneNum: String
    var EMail: String
    
    
    
    init(with new: Animal.MissingModel) {
        self.imageViewModel = ImageViewModel(urlString: new.picture)
        self.chipNum = new.chipNum
        self.petName = new.petName
        self.petCategory = new.petCategory
        self.gender = new.gender
        self.variety = new.variety
        self.coat = new.coat
        self.exterior = new.exterior
        self.feature = new.feature
        self.lostTime = new.lostTime
        self.lostPlace = new.lostPlace
        self.feederName = new.feederName
        self.phoneNum = new.phoneNum
        self.EMail = new.EMail
    }
    
}

extension AnimalFoundTableViewCellViewModel: Equatable{
    
    static func == (lhs: AnimalFoundTableViewCellViewModel, rhs: AnimalFoundTableViewCellViewModel) -> Bool {
        return lhs.imageViewModel.urlString == rhs.imageViewModel.urlString
    }
    
    
}



extension AnimalFoundTableViewCellViewModel{
    
    func showDetailVC(){
        if let navC = UIApplication.shared.visibleViewController as? UINavigationController{
            let viewModel = AnimalFoundDetailViewModel(viewModel: self)
            let vcAnimalDetail = AnimalFoundDetailViewController(viewModel: viewModel)
            navC.pushViewController(vcAnimalDetail, animated: true)
        }else if let navC = UIApplication.shared.visibleViewController?.navigationController{
            let viewModel = AnimalFoundDetailViewModel(viewModel: self)
            let vcAnimalDetail = AnimalFoundDetailViewController(viewModel: viewModel)
            navC.pushViewController(vcAnimalDetail, animated: true)
        }
    }
    
}


extension AnimalFoundTableViewCellViewModel{
    
    func getAnimalDescription() -> String?{
        var str: String = ""
        if coat.count > 0{
            str += coat + ", "
        }
        if exterior.count > 0{
            str += exterior + ", "
        }
        if variety.count > 0{
            str += variety
        }
        return str
    }
    
}
