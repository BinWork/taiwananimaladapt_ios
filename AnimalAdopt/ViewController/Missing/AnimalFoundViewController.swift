//
//  AnimalFoundViewController.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/13.
//

import UIKit

class AnimalFoundViewController: BaseViewController {

    let viewModel = AnimalFoundViewModel()
    
    let tableView: AnimalFoundTableView = {
        return AnimalFoundTableView(frame: .zero, style: .plain)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addUI()
        setup()
    }
    

    private func addUI(){
        
        view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            if #available(iOS 11.0, *) {
                make.top.equalTo(view.safeAreaLayoutGuide).offset(snpOffset.top)
            } else {
                make.top.equalToSuperview().offset(40)
            }
            make.left.equalToSuperview().offset(snpOffset.left)
            make.right.equalToSuperview().offset(snpOffset.right)
            make.bottom.equalToSuperview().offset(snpOffset.bottom * 2)
        }
        
    }

}

extension AnimalFoundViewController{
    
    private func setup(){
        viewModel.onCellModelsGet = { [weak self] (cellModels, isReloadAnimated) in
            guard let sf = self else { return }
            DispatchQueue.main.async {
                sf.tableView.viewModels = cellModels
                sf.tableView.baseTableViewViewModel.isReloadAnimated = isReloadAnimated
                sf.tableView.reloadData()
            }
        }
        
        viewModel.updatePetLostList()
        
        tableView.baseTableViewViewModel.isHeaderRefresh = true
        tableView.baseTableViewViewModel.onHeaderPullRefresh = { [weak self] in
            guard let sf = self else { return }
            DispatchQueue.main.async {
                sf.viewModel.updatePetLostList()
            }
        }
        
        tableView.baseTableViewViewModel.isFooterRefresh = true
        tableView.baseTableViewViewModel.onFooterPullRefresh = {[weak self] in
            guard let sf = self else { return }
            DispatchQueue.main.async {
                let currentCellCount = sf.viewModel.cellModels.count
                sf.viewModel.updatePetLostList(top: 10, skip: currentCellCount)
            }
        }
        
    }
    
}
