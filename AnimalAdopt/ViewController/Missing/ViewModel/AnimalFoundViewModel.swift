//
//  AnimalFoundViewModel.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/13.
//

import Foundation

class AnimalFoundViewModel{
    var cellModels: [AnimalFoundTableViewCellViewModel] = []
    var onCellModelsGet: (([AnimalFoundTableViewCellViewModel], _ isReloadAnimationNeed: Bool) -> Void)?
}

extension AnimalFoundViewModel{
    
    func updatePetLostList(top: Int = 20, skip: Int = 0){
        let request = PetLoseRequest(top: top, skip: skip, parameter: nil)
        Enviroment.apiManager.getPetLostList(request: request) {  [weak self] (result) in
            guard let sf = self else { return }
            DispatchQueue.main.async {
                switch result{
                case .failure(let err):
                    ErrorManager.shared.handle(err)
                case .success(let data):
                    sf.convertToAnimalFoundTableViewCell(with: data, skip: skip)
                    if let cellModelGet = sf.onCellModelsGet{
                        let isReloadAnimationNeed: Bool = skip == 0 ? true : false
                        cellModelGet(sf.cellModels, isReloadAnimationNeed)
                    }
                }
            }
        }
    }
    
    private func convertToAnimalFoundTableViewCell(with data: [Animal.MissingModel], skip: Int){
        if skip == 0{
            cellModels.removeAll()
        }
        for d in data{
            let newCellModel = AnimalFoundTableViewCellViewModel(with: d)
            cellModels.append(newCellModel)
        }
        cellModels = cellModels.removeDuplicates()
    }
    
}
