//
//  HomeTableView.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/06.
//

import UIKit

class HomeTableView: BaseTableView {
    
    private let viewModel = HomeTableViewViewModel()
    
    private let cellClass = HomeTableViewCell.self
    private let cellID = HomeTableViewCell.cellID
    private let cellHeight: CGFloat = CGFloat(20)
    
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        
        setupBaseTableViewViewModel()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupBaseTableViewViewModel(){
        baseTableViewViewModel.isReloadAnimated = false
        
        baseTableViewViewModel.setProperties(
            dataSource: self, delegate: self,
            cellClass: cellClass, cellReuseId: cellID
        )
    }
    
    
    
}





extension HomeTableView: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.models.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let index = indexPath.section
        
        guard let _viewModel = viewModel.models[safe: index] else{
            return UITableViewCell()
        }
        
        if let cell = tableView.dequeueReusableCell(
            withIdentifier: cellID,
            for: indexPath)
            as? HomeTableViewCell{
            
            cell.setup(with: _viewModel)
            
            return cell
        }
        
        return UITableViewCell()
    }
}



extension HomeTableView: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let _viewModel = viewModel.models[safe: indexPath.section] else { return }
        _viewModel.clickAction()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let v = UIView()
        v.backgroundColor = .clear
        return v
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        5
    }
}
