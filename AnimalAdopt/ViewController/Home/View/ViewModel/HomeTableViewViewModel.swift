//
//  HomeTableViewViewModel.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/12.
//

import Foundation

class HomeTableViewViewModel{
    
    var models: [HomeTableViewCellViewModel]
    
    init() {
        models = APIs.allCases.map({ (api) -> HomeTableViewCellViewModel in
            return HomeTableViewCellViewModel(viewModel: api)
        })
    }
    
}

