//
//  HomeTableViewCellViewModel.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/12.
//

import Foundation
import UIKit.UIApplication

class HomeTableViewCellViewModel{
    
    var api: APIs
    
    init(viewModel: APIs) {
        self.api = viewModel
    }
    
}


extension HomeTableViewCellViewModel{
    
    func clickAction(){
        if let navC = UIApplication.shared.visibleViewController?.navigationController{
            switch api{
            case .animalRecognition:
                let vcAnimalAdopt = AnimalAdoptViewController()
                navC.pushViewController(vcAnimalAdopt, animated: true)
            case .petLoseList:
                let vcAnimalFound = AnimalFoundViewController()
                navC.pushViewController(vcAnimalFound, animated: true)
            }
        }
    }
    
}
