//
//  HomeTableViewCell.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/06.
//

import UIKit

class HomeTableViewCell: BaseTableViewCell {
    
    static var cellID: String = String(describing: self)
    
    var viewModel: HomeTableViewCellViewModel?
    
    var lblTitle: UILabel = {
        let lbl = UILabel()
        lbl.adjustsFontSizeToFitWidth = true
        lbl.minimumScaleFactor = 0.5
        if #available(iOS 11.0, *) {
            lbl.font = UIFontMetrics(forTextStyle: .largeTitle).scaledFont(for: .systemFont(ofSize: 18))
        }
        return lbl
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        clipsToBounds = true
        makeCorner(radius: 10)
        makeBorder(width: 1, color: .defaultTextColor)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addUI()
    }
    
    private func addUI(){
        
        contentView.addSubview(lblTitle)
        lblTitle.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(snpOffset.top)
            make.left.equalToSuperview().offset(snpOffset.left * 2)
            make.right.equalToSuperview().offset(snpOffset.right * 2)
            make.bottom.equalToSuperview().offset(snpOffset.bottom)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension HomeTableViewCell{
    
    func setup(with newViewModel: HomeTableViewCellViewModel){
        viewModel = newViewModel
        guard let _viewModel = viewModel else { return }
        
    }
    
    
}
