//
//  HomeViewController.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/05.
//

import UIKit
import SDWebImage
class HomeViewController: BaseViewController {

    private let viewModel = HomeViewControllerViewModel()
    
    let tableView: HomeTableView = {
        return HomeTableView(frame: .zero, style: .plain)
    }()
    
    let btnSetting: BaseButton = {
        let btn = BaseButton()
        let btnImage = UIImage(named: ImageName.Button.setting)?.withRenderingMode(.alwaysOriginal)
        btn.setImage(btnImage, for: .normal)
        btn.imageView?.contentMode = .scaleAspectFit
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addUI()
        addNavigationRightItems(button: btnSetting)
        setup()
        let asdfsa: [String] = []
        
    }
    

    private func addUI(){
        
        view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            if #available(iOS 11.0, *) {
                make.top.equalTo(view.safeAreaLayoutGuide).offset(snpOffset.top)
            } else {
                make.top.equalToSuperview().offset(40)
            }
            make.left.equalToSuperview().offset(snpOffset.left)
            make.right.equalToSuperview().offset(snpOffset.right)
            make.bottom.equalToSuperview().offset(snpOffset.bottom * 2)
        }
        
    }

}

extension HomeViewController{
    
    private func setup(){
        btnSetting.viewModel.onTouch = { [weak self] in
            guard let sf = self else { return }
            sf.viewModel.showSideMenu()
        }
    }
    
}
