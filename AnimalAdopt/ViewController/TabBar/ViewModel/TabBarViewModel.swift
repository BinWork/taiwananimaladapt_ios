//
//  TabBarViewModel.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/14.
//

import Foundation
import UIKit.UIViewController

class TabBarViewModel{
    
    var viewControllers: [UIViewController] = []
    var tabs: [Tabs] = [
        .Adopt,
        .LostAndFound,
        .Favorite,
    ]
    
    enum Tabs: CaseIterable{
        case Adopt
        case Home
        case LostAndFound
        case Favorite
        
        var title: String{
            switch self {
            case .Home:
                return Localizations.FooterBar.Home.title
            case .Adopt:
                return Localizations.FooterBar.Adopt.title
            case .LostAndFound:
                return Localizations.FooterBar.Missing.title
            case .Favorite:
                return Localizations.FooterBar.Favorite.title
            }
        }
        
        var description: String{
            switch self {
            case .Home:
                return Localizations.FooterBar.Home.description
            case .Adopt:
                return Localizations.FooterBar.Adopt.description
            case .LostAndFound:
                return Localizations.FooterBar.Missing.description
            case .Favorite:
                return Localizations.FooterBar.Favorite.description
            }
        }
        
        var icon: UIImage?{
            switch self {
            case .Home:
                return UIImage(named: ImageName.TabBar.home)
            case .Adopt:
                return UIImage(named: ImageName.TabBar.adopt)
            case .LostAndFound:
                return UIImage(named: ImageName.TabBar.missing)
            case .Favorite:
                return UIImage(named: ImageName.TabBar.favorite)
            }
        }
    }
    
    init() {
        generateViewControllers()
    }
    
    private func generateViewControllers(){
        
        tabs.forEach { (tab) in
            var vc: UIViewController = .init()
            switch tab{
            case .Home:
                vc = BaseViewController()
                vc.title = tab.description
                let navC = BaseNavigationController(rootViewController: vc)
                navC.tabBarItem.title = tab.title
                navC.tabBarItem.image = tab.icon
                viewControllers.append(navC)
            case .Adopt:
                vc = AnimalAdoptViewController()
                let navC = BaseNavigationController(rootViewController: vc)
                vc.title = tab.description
                navC.tabBarItem.title = tab.title
                navC.tabBarItem.image = tab.icon
                viewControllers.append(navC)
            case .LostAndFound:
                let vc = AnimalFoundViewController()
                let navC = BaseNavigationController(rootViewController: vc)
                vc.title = tab.description
                navC.tabBarItem.title = tab.title
                navC.tabBarItem.image = tab.icon
                viewControllers.append(navC)
            case .Favorite:
                let vc = FavoriteViewController()
                let navC = BaseNavigationController(rootViewController: vc)
                vc.title = tab.description
                navC.tabBarItem.title = tab.title
                navC.tabBarItem.image = tab.icon
                viewControllers.append(navC)
            }
        }
    }
}
