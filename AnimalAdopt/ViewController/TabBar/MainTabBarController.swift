//
//  MainTabBarController.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/14.
//

import UIKit

class MainTabBarController: UITabBarController {

    private let viewModel = TabBarViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settingTabbarUI()
        setup()
    }

    private func settingTabbarUI(){
        tabBar.tintColor = .peacock
        tabBar.barTintColor = .footerBarBackgroundColor
        tabBar.isTranslucent = false
        if #available(iOS 10.0, *) {
            tabBar.unselectedItemTintColor = .middleGray
        }
        
    }
    
    
    private func setup(){
        viewControllers = viewModel.viewControllers
    }
}
