//
//  SearchTableView.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/08/02.
//

import UIKit

class SearchTableView: BaseTableView {
    
    var viewModels: [SearchTableViewCellViewModel] = []
    
    private let cellClass = SearchTableViewCell.self
    private let cellID = SearchTableViewCell.cellID
    private let cellHeight: CGFloat = CGFloat(20)
    
    
    
    override var contentSize:CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }

    override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        
        allowsSelection = false
        
        backgroundColor = .clear
        
        baseTableViewViewModel.isReloadAnimated = false
        baseTableViewViewModel.setProperties(
            dataSource: self, delegate: self,
            cellClass: cellClass, cellReuseId: cellID
        )
    }
    
    
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}





extension SearchTableView: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModels.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let index = indexPath.section
        
        guard let _viewModel = viewModels[safe: index] else{
            let blankCell = UITableViewCell()
            blankCell.backgroundColor = .clear
            return blankCell
        }
        
        if let cell = tableView.dequeueReusableCell(
            withIdentifier: cellID,
            for: indexPath)
            as? SearchTableViewCell{
            
            cell.viewModel = nil
            cell.viewModel = _viewModel
            
            cell.tag = _viewModel.tag
            cell.setup()
            return cell
        }
        
        return UITableViewCell()
    }
}



extension SearchTableView: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let baseCellHeight: CGFloat = 50
        if let _viewModel = viewModels[safe: indexPath.section]{
            switch _viewModel.conditionType{
            case .area:
                let itemEachRow: CGFloat = 3
                var numOfRow = CGFloat(AreaPkId.allCases.count) / itemEachRow
                numOfRow += numOfRow.truncatingRemainder(dividingBy: itemEachRow) > 0 ? CGFloat(1) : CGFloat(0)
                return numOfRow * baseCellHeight
            default:
                return baseCellHeight
            }
        }
        return baseCellHeight
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let _viewModel = viewModels[safe: section] else {
            let v = UIView()
            v.backgroundColor = .clear
            return v
        }
        let v = UILabel()
        v.text = _viewModel.titleHeader
        v.backgroundColor = .clear
        return v
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
}
