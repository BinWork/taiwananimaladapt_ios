//
//  SearchConditionCollectionView.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/08/03.
//

import UIKit

class SearchConditionCollectionView: UICollectionView {
    
    private let cellClass = SearchConditionCollectionViewCell.self
    private let cellID = SearchConditionCollectionViewCell.cellID
    private let cellHeight: CGFloat = CGFloat(20)
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        
        allowsSelection = false
        
        dataSource = self
        delegate = self
    }
    
    
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
