//
//  SearchConditionCollectionView.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/08/03.
//

import UIKit

class SearchConditionCollectionView: UICollectionView {
    
    var viewModesls: [SearchConditionCollectionViewCellViewModel] = []
    
    private let cellClass = SearchConditionCollectionViewCell.self
    private let cellID = SearchConditionCollectionViewCell.cellID
    
    private lazy var cellSize_SingleSelection: CGSize = {
        let screenWidth: CGFloat = UIScreen.main.bounds.size.width
        let cellWidth = screenWidth * 0.165
        let cellHeight = CGFloat(40)
        return CGSize(width: cellWidth, height: cellHeight)
    }()
    
    private let cellSize_MutipleSelection: CGSize = {
        let screenWidth: CGFloat = UIScreen.main.bounds.size.width
        let cellWidth = screenWidth * 0.228
        let cellHeight = CGFloat(40)
        return CGSize(width: cellWidth, height: cellHeight)
    }()
    
    private let itemSpacing: CGFloat = 10
    private let lineSpacing: CGFloat = 10

    
    private lazy var cvLayout: UICollectionViewFlowLayout = {
        let l = UICollectionViewFlowLayout()
        l.scrollDirection = .vertical
        l.itemSize = CGSize(width: 50, height: 80)
        l.sectionInset = UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
        l.minimumLineSpacing = lineSpacing
        return l
    }()
    
    
    
    override var contentSize:CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }

    override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
    
    
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        
        backgroundColor = .clear
        allowsSelection = true
        allowsMultipleSelection = false
        
        register(cellClass, forCellWithReuseIdentifier: cellID)
        collectionViewLayout = cvLayout
        dataSource = self
        delegate = self
    }

    
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


extension SearchConditionCollectionView: UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModesls.count
    }
    
    override func numberOfItems(inSection section: Int) -> Int {
        return viewModesls.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let _viewModel = viewModesls[safe: indexPath.row] else{
            let cell = UICollectionViewCell()
            cell.backgroundColor = .green
            return cell
        }
        
        if let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: cellID, for: indexPath
        ) as? SearchConditionCollectionViewCell{
            cell.viewModel = _viewModel
            cell.setup()
            return cell
        }
        
        
        let cell = UICollectionViewCell()
        cell.backgroundColor = .red
        return cell
        
    }
    
    
}

extension SearchConditionCollectionView: UICollectionViewDelegateFlowLayout{
 
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        
        if let _viewModel = viewModesls[safe: indexPath.row]{
            _viewModel.didConditionChanged()
        }
        
        if let indexPaths = collectionView.indexPathsForSelectedItems,
           indexPaths.contains(indexPath) {
            collectionView.deselectItem(at: indexPath, animated: false)
            return false
        }
        collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .init())
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldDeselectItemAt indexPath: IndexPath) -> Bool {

        if let _viewModel = viewModesls[safe: indexPath.row]{
            _viewModel.didConditionChanged()
        }
        return true
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if allowsMultipleSelection{
            return cellSize_MutipleSelection
        }
        return cellSize_SingleSelection
    }
    
    
    
}
