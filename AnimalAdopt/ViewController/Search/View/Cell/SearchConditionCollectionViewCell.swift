//
//  SearchConditionCollectionViewCell.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/08/03.
//

import UIKit

class SearchConditionCollectionViewCell: BaseCollectionViewCell {
    
    weak var viewModel: SearchConditionCollectionViewCellViewModel?
    
    static var cellID: String = String(describing: self)
    
    let lblTitle: UILabel = {
        let v = UILabel()
        v.textColor = .defaultTextColor
        v.textAlignment = .center
        v.baselineAdjustment = .alignCenters
        v.minimumScaleFactor = 0.6
        v.adjustsFontSizeToFitWidth = true
        v.numberOfLines = 0
        v.lineBreakMode = .byClipping
        return v
    }()
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        makeCornerRound()
        makeBorder(width: 2, color: .defaultTextColor)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        addUI()
        updateUI()
    }
    
    override var isSelected: Bool{
        didSet{
            updateUI()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

extension SearchConditionCollectionViewCell{
    
    private func addUI(){
        addSubview(lblTitle)
        lblTitle.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(snpOffset.top)
            make.left.equalToSuperview().offset(snpOffset.left * 2)
            make.right.equalToSuperview().offset(snpOffset.right * 2)
            make.bottom.equalToSuperview().offset(snpOffset.bottom)
        }
    }
    
    private func updateUI(){
        backgroundColor = isSelected ? .defaultTextColor : .clear
        lblTitle.textColor = isSelected ? .defaultTextColor_Selected : .defaultTextColor
        lblTitle.font = isSelected ? .boldSystemFont(ofSize: 17) : .systemFont(ofSize: 17)
    }
    
}

extension SearchConditionCollectionViewCell{
    
    func setup(){
        guard let _viewModel = viewModel else { return }
        var title: String?
        
        switch _viewModel.option{
        case let s as Animal.Kind:
            title = s.string
        case let s as Animal.Age:
            title = s.string
        case let s as Animal.BodyType:
            title = s.string
        case let s as Animal.Gender:
            title = s.string
        case let s as Animal.Boolean:
            title = s.string
        case let areaPkId as AreaPkId:
            title = areaPkId.string
        default:
            break
        }
        
        lblTitle.text = title
    }
    
}

