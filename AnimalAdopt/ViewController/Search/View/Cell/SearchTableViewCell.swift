//
//  SearchTableViewCell.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/08/02.
//

import UIKit

class SearchTableViewCell: BaseTableViewCell {
    
    weak var viewModel: SearchTableViewCellViewModel?{
        willSet{
            if viewModel != newValue{
                contentView.subviews.forEach({ $0.removeFromSuperview() })
            }
        }
    }
    
    static var cellID: String = String(describing: self)
    
    let collectionView: SearchConditionCollectionView = {
        let v = SearchConditionCollectionView(frame: .zero, collectionViewLayout: .init())
        return v
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

extension SearchTableViewCell{
    
    func setup(){
        guard let _viewModel = viewModel else { return }
        
        contentView.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            make.top.left.right.bottom.equalToSuperview()
        }
        
        collectionView.viewModesls = _viewModel.cvCellViewModels
        switch _viewModel.conditionType {
        case .area:
            collectionView.allowsMultipleSelection = true
        default:
            collectionView.allowsMultipleSelection = false
        }
        collectionView.reloadData()

    }
    
}

