//
//  SearchConditionTableViewCell.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/08/03.
//

import UIKit

class SearchConditionCollectionViewCell: UITableViewCell {
    
    weak var viewModel: SearchConditionCollectionViewCell?{
        willSet{
            if viewModel != newValue{
                contentView.subviews.forEach({ $0.removeFromSuperview() })
            }
        }
    }
    
    static var cellID: String = String(describing: self)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension SearchConditionCollectionViewCell{
    
}

extension SearchConditionCollectionViewCell{
    
    func setup(){
        guard let _viewModel = viewModel else { return }
    }
    
}

