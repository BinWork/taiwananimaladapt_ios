//
//  SearchTableViewCellViewModel.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/08/02.
//

import Foundation

class SearchTableViewCellViewModel: Equatable{
    
    var conditionType: SearchViewControllerViewModel.ConditionType
    var titleHeader: String?
    var tag: Int
    
    var cvCellViewModels: [SearchConditionCollectionViewCellViewModel] = []
    
    
    init(conditionType: SearchViewControllerViewModel.ConditionType){
        self.conditionType = conditionType
        self.titleHeader = conditionType.title
        self.tag = conditionType.rawValue
        
        cvCellViewModels = generateCollectionViewCellViewModels()
    }
    
}

extension SearchTableViewCellViewModel{
    static func == (lhs: SearchTableViewCellViewModel, rhs: SearchTableViewCellViewModel) -> Bool {
        return lhs.conditionType == rhs.conditionType
    }
}

extension SearchTableViewCellViewModel{

    
    
    private func generateCollectionViewCellViewModels() -> [SearchConditionCollectionViewCellViewModel]{
        let options: [Any] = generateOptions()
        var _viewModels: [SearchConditionCollectionViewCellViewModel] = []
        options.forEach { (option) in
            let _viewModel = SearchConditionCollectionViewCellViewModel(
                option: option, conditionType: conditionType
            )
            _viewModels.append(_viewModel)
        }
        return _viewModels
    }
    
    
    private func generateOptions() -> [Any]{
        switch conditionType{
        case .kind:
            return Animal.Kind.allCases
        case .age:
            return [
                Animal.Age.child,
                Animal.Age.adult
            ]
        case .bodyType:
            return Animal.BodyType.allCases
        case .gender:
            return [
                Animal.Gender.male,
                Animal.Gender.female
            ]
        case .sterilization:
            return [
                Animal.Boolean.yes
            ]
        case .bacterin:
            return [
                Animal.Boolean.yes
            ]
        case .area:
            return AreaPkId.allCases
        }
    }
}
