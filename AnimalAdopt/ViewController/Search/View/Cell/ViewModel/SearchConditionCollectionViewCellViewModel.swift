//
//  SearchConditionCollectionViewCellViewModel.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/08/03.
//

import Foundation

class SearchConditionCollectionViewCellViewModel{
    
    var conditionType: SearchViewControllerViewModel.ConditionType
    var option: Any
    
    init(option: Any, conditionType: SearchViewControllerViewModel.ConditionType){
        self.option = option
        self.conditionType = conditionType
    }
    
}

extension SearchConditionCollectionViewCellViewModel{

    func didConditionChanged(){
        let userInfo: [String: Any] = [
            NotificationName.Search.key: self
        ]
        NotificationCenter.default.post(
            name: NotificationName.Search.didConditionChanged,
            object: nil, userInfo: userInfo
        )
    }
    
}
