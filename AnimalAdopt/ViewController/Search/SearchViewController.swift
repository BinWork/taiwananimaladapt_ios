//
//  SearchViewController.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/28.
//

import UIKit

class SearchViewController: BaseViewController {

    let viewModel = SearchViewControllerViewModel()

    private let scrollView: UIScrollView = {
        let v = UIScrollView()
        return v
    }()
    
    private let btnExit: BaseButton = {
        let btn = BaseButton()
        let btnImage = UIImage(named: ImageName.Button.cross)
        btn.setImage(btnImage, for: .normal)
        btn.imageView?.contentMode = .scaleAspectFit
        return btn
    }()
    
    private let btnResetCondition: BaseButton = {
        let btn = BaseButton()
        btn.setTitle(Localizations.System.Button.reset, for: .normal)
        btn.setTitleColor(.defaultTextColor_Selected, for: .normal)
        btn.backgroundColor = .peacock
        let btnImage = UIImage(named: ImageName.Button.clear)
        btn.setImage(btnImage, for: .normal)
        btn.imageView?.contentMode = .scaleAspectFit
        btn.imageEdgeInsets = UIEdgeInsets(top: 0, left: -15, bottom: 0, right: 0)
        return btn
    }()
    
    private let btnStartSearch: BaseButton = {
        let btn = BaseButton()
        btn.setTitle(Localizations.System.Button.confirm, for: .normal)
        btn.setTitleColor(.defaultTextColor_Selected, for: .normal)
        btn.backgroundColor = .peacock
        return btn
    }()
    
    private let tableView: SearchTableView = {
        return SearchTableView()
    }()
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        btnResetCondition.makeCornerRound()
        btnStartSearch.makeCornerRound()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .defaultBackgroundColor
        addNavigationRightItems(button: btnExit)
        addUI()
        setup()
    }

}

extension SearchViewController{
    
    private func addUI(){
        view.addSubview(scrollView)
        scrollView.snp.makeConstraints { (make) in
            if #available(iOS 11.0, *) {
                make.top.equalTo(view.safeAreaLayoutGuide)
            } else {
                make.top.equalTo(navigationBarHeight)
            }
            make.bottom.equalToSuperview()
            make.width.equalToSuperview()
        }
        
        scrollView.addSubview(btnResetCondition)
        btnResetCondition.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(snpOffset.top * 3)
            make.left.equalTo(view).offset(snpOffset.left * 5)
            make.width.equalTo(view).multipliedBy(0.272)
            make.height.equalTo(btnResetCondition.snp.width).multipliedBy(0.3235)
        }
        
        scrollView.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(btnResetCondition.snp.bottom).offset(snpOffset.top * 2)
            make.left.equalTo(btnResetCondition)
            make.width.equalTo(view).multipliedBy(0.787)
        }
        
        scrollView.addSubview(btnStartSearch)
        btnStartSearch.snp.makeConstraints { (make) in
            make.top.equalTo(tableView.snp.bottom).offset(snpOffset.top * 2).priority(800)
            make.left.equalTo(tableView)
            make.right.equalTo(view).offset(snpOffset.right * 5)
            make.bottom.equalToSuperview().offset(snpOffset.bottom * 3)
            make.height.equalTo(btnStartSearch.snp.width).multipliedBy(0.135)
        }
        
        
        
    }
    
}




extension SearchViewController{
    
    private func setup(){
        btnExit.viewModel.onTouch = { [weak self] in
            guard let sf = self else { return }
            sf.dismiss(animated: true, completion: nil)
        }
        
        btnResetCondition.viewModel.onTouch = { [weak self] in
            guard let sf = self else { return }
            sf.viewModel.resetCondition()
        }
        
        btnStartSearch.viewModel.onTouch =  { [weak self] in
            guard let sf = self else { return }
            sf.viewModel.startSearch {
                sf.dismiss(animated: true, completion: nil)
            }
        }
        
        tableView.viewModels = viewModel.cellViewModels
        tableView.reloadData()
    }
    
}
