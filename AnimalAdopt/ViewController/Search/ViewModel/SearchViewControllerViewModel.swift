//
//  SearchViewControllerViewModel.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/28.
//

import Foundation

class SearchViewControllerViewModel{
    
    enum ConditionType: Int, CaseIterable{
        case kind = 0
        case age
        case bodyType
        case gender
        case sterilization
        case bacterin
        case area
        
        var title: String{
            switch self{
            case .kind:
                return Localizations.Animal.Data.Kind.title
            case .age:
                return Localizations.Animal.Data.Age.title
            case .bodyType:
                return Localizations.Animal.Data.BodyType.title
            case .gender:
                return Localizations.Animal.Data.Gender.title
            case .sterilization:
                return Localizations.Animal.Data.Sterilization.title
            case .bacterin:
                return Localizations.Animal.Data.backterin
            case .area:
                return Localizations.Animal.Data.areaPkId
            }
        }
    }

    weak var delegate: SearchViewControllerDelegate?

    var searchConditions: [ConditionType] = ConditionType.allCases
    var cellViewModels: [SearchTableViewCellViewModel] = []
    
    var objSearchModel: Animal.SearchModel = .init()
    var searchConditionObserver: NSObjectProtocol?
    
    
    init() {
        generateCellViewModels()
        searchConditionObserver = getSearchConditionObserver()
    }
    
    deinit {
        if let searchConditionObserver = searchConditionObserver{
            NotificationCenter.default.removeObserver(
                searchConditionObserver, name: NotificationName.Search.didConditionChanged,
                object: nil
            )
        }
    }
}

extension SearchViewControllerViewModel{
    
    private func generateCellViewModels(){
        cellViewModels.removeAll()
        cellViewModels = ConditionType.allCases.map({ (type) -> SearchTableViewCellViewModel in
            return SearchTableViewCellViewModel(conditionType: type)
        })
    }
    
}

extension SearchViewControllerViewModel{
    
    func startSearch(_ action: (()->())?){
        if let _action = action{
            delegate?.search(object: objSearchModel)
            _action()
        }
    }
    
    func resetCondition(){
        objSearchModel = .init()
    }
    
    private func getSearchConditionObserver() -> NSObjectProtocol{
        return NotificationCenter.default.addObserver(
            forName: NotificationName.Search.didConditionChanged,
            object: nil, queue: nil, using: { [weak self] (notif) in
            guard let sf = self else { return }
            guard let viewModel = notif.userInfo?[NotificationName.Search.key] as? SearchConditionCollectionViewCellViewModel else { return }
                sf.setSearchConditions(with: viewModel)
        })
    }
    
    private func setSearchConditions(with viewModel: SearchConditionCollectionViewCellViewModel){
        switch viewModel.conditionType{
        case .kind:
            if let kind = viewModel.option as? Animal.Kind{
                objSearchModel.animal_kind = objSearchModel.animal_kind == kind ? nil : kind
            }
        case .age:
            if let age = viewModel.option as? Animal.Age{
                objSearchModel.animal_age = objSearchModel.animal_age == age ? nil : age
            }
        case .bodyType:
            if let bodyType = viewModel.option as? Animal.BodyType{
                objSearchModel.animal_bodytype = objSearchModel.animal_bodytype == bodyType ? nil : bodyType
            }
        case .gender:
            if let gender = viewModel.option as? Animal.Gender{
                objSearchModel.animal_sex = objSearchModel.animal_sex == gender ? nil : gender
            }
        case .sterilization:
            if let sterilization = viewModel.option as? Animal.Boolean{
                objSearchModel.animal_sterilization = objSearchModel.animal_sterilization == sterilization ? nil : sterilization
            }
        case .bacterin:
            if let bacterin = viewModel.option as? Animal.Boolean{
                objSearchModel.animal_bacterin = objSearchModel.animal_bacterin == bacterin ? nil : bacterin
            }
        case .area:
            guard let area = viewModel.option as? AreaPkId else { return }
            if objSearchModel.animal_area_pkid.contains(area){
                objSearchModel.animal_area_pkid = objSearchModel.animal_area_pkid.filter({$0 != area})
            }else{
                objSearchModel.animal_area_pkid.append(area)
            }
        }
    }
    
}
