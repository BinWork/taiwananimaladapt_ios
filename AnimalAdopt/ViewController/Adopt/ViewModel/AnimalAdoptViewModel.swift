//
//  AnimalAdoptViewModel.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/12.
//

import Foundation
import UIKit

class AnimalAdoptViewModel{
    
    var cellModels: [AnimalAdoptTableViewCellViewModel] = []
    
    var onCellModelsGet: (([AnimalAdoptTableViewCellViewModel], _ isReloadAnimationNeed: Bool) -> Void)?

}

extension AnimalAdoptViewModel{
    
    
    func updateAnimalAdoptList(top: Int = 20, skip: Int = 0){
        let request = AdoptRequest(top: top, skip: skip, parameter: nil)
        Enviroment.apiManager.getAnimalAdoptList(request: request) {  [weak self] (result) in
            guard let sf = self else { return }
            DispatchQueue.main.async {
                switch result{
                case .failure(let err):
                    ErrorManager.shared.handle(err)
                case .success(let models):

                    sf.handleModels(models, should: skip)

                    if let cellModelGet = sf.onCellModelsGet{
                        let isReloadAnimationNeed: Bool = skip == 0 ? true : false
                        cellModelGet(sf.cellModels, isReloadAnimationNeed)
                    }
                }
            }
        }
    }
    
    private func handleModels(_ models: [Animal.RecognitionModel], should skip: Int){
        if skip == 0{
            cellModels.removeAll()
        }
        let newCellModels = convertToCellModel(with: models)
        cellModels += newCellModels
        cellModels = cellModels.removeDuplicates()
    }
    
    private func convertToCellModel(with data: [Animal.RecognitionModel]) -> [AnimalAdoptTableViewCellViewModel]{
        return data.map { (d) -> AnimalAdoptTableViewCellViewModel in
            return AnimalAdoptTableViewCellViewModel(model: d)
        }
    }
    
}

extension AnimalAdoptViewModel{
    
    func gotoSearchViewController(){
        if let visibleViewController = UIApplication.shared.visibleViewController{
            let searchVC = SearchViewController()
            searchVC.title = Localizations.System.Search.title
            searchVC.viewModel.delegate = self
            let navC = BaseNavigationController(rootViewController: searchVC)
            navC.modalPresentationStyle = .overFullScreen
            visibleViewController.present(navC, animated: true, completion: nil)
        }
    }
    
}


extension AnimalAdoptViewModel{
    
    private func prepareSearch(with model: Animal.SearchModel, top: Int = 20, skip: Int = 0){
        track(getQueries(with: model))
    }
    
    private func getQueries(with model: Animal.SearchModel) -> [String: String]{
        var queries: [String: String] = [:]
        
        let key = Animal.SearchModel.Key.self
        if let value = model.animal_id{
            queries[key.animal_id.rawValue] = value.toString
        }
        if let value = model.animal_subid{
            queries[key.animal_subid.rawValue] = value
        }
//        if let areaPkId = model.animal_area_pkid{
//            queries[key.animal_area_pkid.rawValue] = areaPkId.toString
//        }
        if let value = model.animal_shelter_pkid{
            queries[key.animal_shelter_pkid.rawValue] = value.toString
        }
        if let value = model.animal_place{
            queries[key.animal_place.rawValue] = value
        }
        if let value = model.animal_kind{
            queries[key.animal_kind.rawValue] = value.rawValue
        }
        if let value = model.animal_sex{
            queries[key.animal_sex.rawValue] = value.rawValue
        }
        if let value = model.animal_bodytype{
            queries[key.animal_bodytype.rawValue] = value.rawValue
        }
        if let value = model.animal_colour{
            queries[key.animal_colour.rawValue] = value
        }
        if let value = model.animal_age{
            queries[key.animal_age.rawValue] = value.rawValue
        }
        if let value = model.animal_sterilization{
            queries[key.animal_sterilization.rawValue] = value.rawValue
        }
        if let value = model.animal_bacterin{
            queries[key.animal_bacterin.rawValue] = value.rawValue
        }
        if let value = model.animal_foundplace{
            queries[key.animal_foundplace.rawValue] = value
        }
        if let value = model.animal_status{
            queries[key.animal_status.rawValue] = value.rawValue
        }
        if let value = model.animal_opendate{
            queries[key.animal_opendate.rawValue] = value
        }
        if let value = model.animal_closeddate{
            queries[key.animal_closeddate.rawValue] = value
        }
        if let value = model.animal_update{
            queries[key.animal_update.rawValue] = value
        }
        if let value = model.animal_createtime{
            queries[key.animal_createtime.rawValue] = value
        }
        if let value = model.shelter_name{
            queries[key.shelter_name.rawValue] = value
        }
        if let value = model.cDate{
            queries[key.cDate.rawValue] = value
        }
        if let value = model.shelter_address{
            queries[key.shelter_address.rawValue] = value
        }
        
        return queries
    }
    
    
}

extension AnimalAdoptViewModel: SearchViewControllerDelegate{

    func search(object: Animal.SearchModel){
        prepareSearch(with: object)
    }
}
