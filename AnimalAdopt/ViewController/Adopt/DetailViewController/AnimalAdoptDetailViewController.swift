//
//  AnimalAdoptDetailViewController.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/07.
//

import UIKit

class AnimalAdoptDetailViewController: BaseAnimalDetailViewController {

    var viewModel: AnimalAdoptDetailViewModel
    
    let viewStatus: AnimalStatusView = {
        let v = AnimalStatusView()
        return v
    }()
    
    let stackAnimalDetailed: AnimalDetailedView = {
        let v = AnimalDetailedView()
        v.backgroundColor = .defaultBackgroundColor
        return v
    }()
    
    let stackAnimalShelter: AnimalDetailedView = {
        let v = AnimalDetailedView()
        v.backgroundColor = .defaultBackgroundColor
        return v
    }()
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imageView.makeBorder(width: .animalImageBorderWidth * 1.5, color: viewModel.gender.color)
        stackAnimalDetailed.makeCorner(radius: 10)
        stackAnimalShelter.makeCorner(radius: 10)
        
    }
    
    init(viewModel new: AnimalAdoptDetailViewModel) {
        self.viewModel = new
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        addUI()
    }

    private func addUI(){
        
        scrollView.addSubview(viewStatus)
        viewStatus.snp.makeConstraints { (make) in
            make.top.equalTo(viewFeature.snp.bottom).offset(snpOffset.top * 2)
            make.centerX.equalTo(view)
            make.width.equalTo(view).multipliedBy(0.6333)
            make.height.equalTo(30)
        }
        
        scrollView.addSubview(stackAnimalDetailed)
        stackAnimalDetailed.snp.makeConstraints { (make) in
            make.top.equalTo(viewStatus.snp.bottom).offset(snpOffset.top * 2)
            make.left.equalTo(view).offset(snpOffset.left * 2)
            make.right.equalTo(view).offset(snpOffset.right * 2)
        }
        
        scrollView.addSubview(stackAnimalShelter)
        stackAnimalShelter.snp.makeConstraints { (make) in
            make.top.equalTo(stackAnimalDetailed.snp.bottom).offset(snpOffset.top * 2)
            make.left.equalTo(view).offset(snpOffset.left * 2)
            make.right.equalTo(view).offset(snpOffset.right * 2)
            make.bottom.equalToSuperview().offset(snpOffset.bottom * 2)
        }
        
    }
    
    
}

extension AnimalAdoptDetailViewController{
    
    
    private func setup(){
        
        title = "ID#" + viewModel.id
        
        lblDate.text = viewModel.createDate + " " + Localizations.Animal.Data.createTime
        
        imageView.viewModel = viewModel.imageViewModel
        
        viewStatus.viewModel = .init(viewModel: viewModel)
        viewStatus.setup()
        
        viewFeature.viewModel = .init(
            kind: viewModel.kind,
            description: viewModel.coat,
            gender: viewModel.gender
        )
        viewFeature.setup()
        
        
        stackAnimalDetailed.viewModel.animalViewModel = viewModel
        stackAnimalDetailed.setup(sections: [.bodyType, .age, .foundPlace, .remark])
        
        stackAnimalShelter.viewModel.animalViewModel = viewModel
        stackAnimalShelter.setup(sections: [.shelterName, .shelterAddress, .openDate, .shelterTel])
        
    }
    
}
