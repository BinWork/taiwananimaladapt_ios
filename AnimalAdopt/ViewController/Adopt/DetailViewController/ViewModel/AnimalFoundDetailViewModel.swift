//
//  AnimalFoundDetailViewModel.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/13.
//

import Foundation
import UIKit.UIColor

class AnimalFoundDetailViewModel{
    
    var imageViewModel: ImageViewModel
    var gender: Animal.Gender
    var kind: Animal.Kind
    var lostTime: String
    var petName: String
    var chip: String
    var feature: String
    var lostPlace: String
    var ownerName: String
    var ownerTel: String
    var ownerEmail: String
    
    init(viewModel: AnimalFoundTableViewCellViewModel) {
        self.imageViewModel = viewModel.imageViewModel
        self.gender = viewModel.gender
        self.kind = viewModel.petCategory
        self.lostTime = viewModel.lostTime
        self.petName = viewModel.petName
        self.chip = viewModel.chipNum
        self.feature = viewModel.feature
        self.lostPlace = viewModel.lostPlace
        self.ownerName = viewModel.feederName
        self.ownerTel = viewModel.phoneNum
        self.ownerEmail = viewModel.EMail
    }
    
    
}
