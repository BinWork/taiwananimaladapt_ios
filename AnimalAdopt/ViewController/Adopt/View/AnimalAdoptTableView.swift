//
//  AnimalAdoptTableView.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/07.
//

import UIKit

class AnimalAdoptTableView: BaseTableView {
    
    
    var viewModels: [AnimalAdoptTableViewCellViewModel] = []
    
    private let cellClass = AnimalAdoptTableViewCell.self
    private let cellID = AnimalAdoptTableViewCell.cellID
    private let cellHeight: CGFloat = CGFloat(20)
    
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        
        baseTableViewViewModel.setProperties(
            dataSource: self, delegate: self,
            cellClass: cellClass, cellReuseId: cellID
        )
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}





extension AnimalAdoptTableView: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModels.count + 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let index = indexPath.section
        
        guard let _viewModel = viewModels[safe: index] else{
            let blankCell = UITableViewCell()
            blankCell.backgroundColor = .clear
            return blankCell
        }
        
        if let cell = tableView.dequeueReusableCell(
            withIdentifier: cellID,
            for: indexPath)
            as? AnimalAdoptTableViewCell{
            
            cell.setup(viewModel: _viewModel, index: index)
            return cell
        }
        
        return UITableViewCell()
    }
}



extension AnimalAdoptTableView: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let viewModel = viewModels[safe: indexPath.section] else { return }
        viewModel.showDetailVC()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let v = UIView()
        v.backgroundColor = .clear
        return v
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        5
    }
}
