//
//  AnimalAdoptTableViewViewModel.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/12.
//

import Foundation
import UIKit.UIImageView

class AnimalAdoptTableViewCellViewModel{
    
    var imageViewModel: ImageViewModel
    
    var id: Int
    var subId: String?
    var areaPkId: Int?
    var shelterPkId: Int?
    var title: String?
    
    var albumFile: String
    var kind: Animal.Kind
    var gender: Animal.Gender
    var bodyType: Animal.BodyType
    var color: String
    var age: Animal.Age
    var sterilization: Animal.Boolean
    var bacterin: Animal.Boolean
    var foundPlace: String
    var status: Animal.Status
    var remark: String
    var openDate: String
    var closeDate: String?
    var albumLastUpdate: String?
    var lastUpdate: String
    var cDate: String?
    
    var shelterName: String?
    var shelterAddress: String?
    var shelterTel: String?
    
    var createTime: String
    
    
    var onFavoriteChanged: ((Bool) -> ())?
    
    init(model: Animal.RecognitionModel) {
        
        imageViewModel = ImageViewModel(urlString: model.album_file)
        
        self.id = model.animal_id
        self.subId = model.animal_subid
        self.areaPkId = model.animal_area_pkid
        self.shelterPkId = model.animal_shelter_pkid
        
        
        self.albumFile = model.album_file
        self.kind = model.animal_kind
        self.gender = model.animal_sex
        self.bodyType = model.animal_bodytype
        self.color = model.animal_colour
        self.age = model.animal_age
        self.sterilization = model.animal_sterilization
        self.bacterin = model.animal_bacterin
        self.foundPlace = model.animal_foundplace
        
        self.status = model.animal_status
        self.remark = model.animal_remark
        self.openDate = model.animal_opendate
        self.closeDate = model.animal_closeddate
        self.albumLastUpdate = model.album_update
        self.lastUpdate = model.animal_update
        self.cDate = model.cDate
        
        
        self.shelterName = model.shelter_name
        self.shelterAddress = model.shelter_address
        self.shelterTel = model.shelter_tel
        
        self.createTime = model.animal_createtime
        
    }
    
    init(model: Animal.FavoriteModel){
        imageViewModel = ImageViewModel(urlString: model.imageUrl)
        
        self.id = Int(model.id) ?? -1
        
        
        self.albumFile = model.imageUrl
        self.kind = model.kind
        self.gender = model.gender
        self.bodyType = model.bodyType
        self.color = model.coat
        self.age = model.age
        self.sterilization = model.sterilization
        self.bacterin = model.bacterin
        self.foundPlace = model.foundPlace
        
        self.status = model.statusAvailable
        self.remark = model.remark
        self.openDate = model.openDate
        self.lastUpdate = model.lastUpdate
        
        self.shelterName = model.shelterName
        self.shelterAddress = model.shelterAddress
        self.shelterTel = model.shelterTel
        
        self.createTime = model.createDate
    }

}



extension AnimalAdoptTableViewCellViewModel: Equatable{
    
    static func == (lhs: AnimalAdoptTableViewCellViewModel, rhs: AnimalAdoptTableViewCellViewModel) -> Bool {
        return lhs.id == rhs.id
    }
    
    
}





extension AnimalAdoptTableViewCellViewModel{
    
    func showDetailVC(){
        if let navC = UIApplication.shared.visibleViewController as? UINavigationController{
            let viewModel = AnimalAdoptDetailViewModel(viewModel: self)
            let vcAnimalDetail = AnimalAdoptDetailViewController(viewModel: viewModel)
            navC.pushViewController(vcAnimalDetail, animated: true)
        }else if let navC = UIApplication.shared.visibleViewController?.navigationController{
            let viewModel = AnimalAdoptDetailViewModel(viewModel: self)
            let vcAnimalDetail = AnimalAdoptDetailViewController(viewModel: viewModel)
            navC.pushViewController(vcAnimalDetail, animated: true)
        }
    }
    
}

extension AnimalAdoptTableViewCellViewModel{
    
    
    /// 將該流浪動物資料加入最愛
    /// - Parameter sender: UIButton
    @objc func toggleFavorite(_ sender: UIButton){
        switch sender.isSelected {
        case true:
            let viewModel = AnimalAdoptDetailViewModel(viewModel: self)
            DataStoreManager.favorite.remove(with: viewModel)
        case false:
            let viewModel = AnimalAdoptDetailViewModel(viewModel: self)
            DataStoreManager.favorite.save(with: viewModel)
        }
    }
    
    
    func updateFavoriteStatus(for button: UIButton, from notif: Notification){
        guard let favorites = notif.userInfo?[NotificationName.Favorites.key] as? [ Animal.FavoriteModel] else { return }
        button.isSelected = favorites.contains(where: {$0.id == String(id)})
    }
    
    func getFavoriteObserver() -> NSObjectProtocol{
        return NotificationCenter.default.addObserver(forName: NotificationName.Favorites.didChanged, object: nil, queue: nil, using: { [weak self] (notif) in
            guard let sf = self else { return }
            guard let favorites = notif.userInfo?[NotificationName.Favorites.key] as? [ Animal.FavoriteModel] else { return }
            if let didFavoriteChange = sf.onFavoriteChanged{
                let a = favorites.contains(where: {$0.id == String(sf.id)})
                didFavoriteChange(a)
            }
        })
    }
}
