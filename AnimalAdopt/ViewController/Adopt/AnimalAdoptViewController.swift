//
//  AnimalAdoptViewController.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/07.
//

import UIKit

class AnimalAdoptViewController: BaseViewController {
    
    let viewModel = AnimalAdoptViewModel()
    
    let btnSearch: BaseButton = {
        let btn = BaseButton()
        let btnImage = UIImage(named: ImageName.Button.search)
        btn.setImage(btnImage, for: .normal)
        btn.imageView?.contentMode = .scaleAspectFit
        return btn
    }()
    
    let tableView: AnimalAdoptTableView = {
        return AnimalAdoptTableView(frame: .zero, style: .plain)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addUI()
        addNavigationRightItems(button: btnSearch)
        setup()
    }

    
    
    deinit{
        
    }

}




extension AnimalAdoptViewController{
    
    private func addUI(){
        view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            if #available(iOS 11.0, *) {
                make.top.equalTo(view.safeAreaLayoutGuide).offset(snpOffset.top)
            } else {
                make.top.equalToSuperview().offset(40)
            }
            make.left.equalToSuperview().offset(snpOffset.left)
            make.right.equalToSuperview().offset(snpOffset.right)
            make.bottom.equalToSuperview().offset(snpOffset.bottom * 2)
        }
    }
}








extension AnimalAdoptViewController{
    
    private func setup(){
        viewModel.onCellModelsGet = { [weak self] (cellModels, isReloadAnimated) in
            guard let sf = self else { return }
            DispatchQueue.main.async {
                sf.tableView.viewModels = cellModels
                sf.tableView.baseTableViewViewModel.isReloadAnimated = isReloadAnimated
                sf.tableView.reloadData()
            }
        }
        
        viewModel.updateAnimalAdoptList()
        
        tableView.baseTableViewViewModel.isHeaderRefresh = true
        tableView.baseTableViewViewModel.onHeaderPullRefresh = { [weak self] in
            guard let sf = self else { return }
            DispatchQueue.main.async {
                sf.viewModel.updateAnimalAdoptList()
            }
        }
        
        tableView.baseTableViewViewModel.isFooterRefresh = true
        tableView.baseTableViewViewModel.onFooterPullRefresh = {[weak self] in
            guard let sf = self else { return }
            DispatchQueue.main.async {
                let currentCellCount = sf.viewModel.cellModels.count
                sf.viewModel.updateAnimalAdoptList(top: 10, skip: currentCellCount)
            }
        }
        
        btnSearch.viewModel.onTouch = { [weak self] in
            guard let sf = self else { return }
            sf.viewModel.gotoSearchViewController()
        }
        
    }
    
}
