//
//  ImageViewController.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/08.
//

import UIKit
import ISVImageScrollView

class ImageViewController: UIViewController {

    let viewModel: ImageViewModel
    
    
    private var imageViewStored: UIImageView = {
        let v = UIImageView()
        v.contentMode = .scaleAspectFit
        return v
    }()
    lazy var animalImageView: ISVImageScrollView = {
        let v = ISVImageScrollView()
        v.maximumZoomScale = 4.0
        v.delegate = self
        v.decelerationRate = .fast
        return v
    }()
    
    init(viewModel: ImageViewModel){
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        setup()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .black
        
        view.addSubview(animalImageView)
        animalImageView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(snpOffset.top * 2)
            make.left.right.equalToSuperview()
            make.bottom.equalToSuperview().offset(snpOffset.bottom * 2)
        }
    }

}

extension ImageViewController{
    
    private func setup(){
        viewModel.onImageGet = { [weak self] img in
            guard let sf = self else { return }
            DispatchQueue.main.async {
                sf.imageViewStored.image = img
                sf.animalImageView.imageView = sf.imageViewStored
            }
        }
        viewModel.getImage()
    }
    
}


extension ImageViewController: UIScrollViewDelegate{
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageViewStored
    }
    
}
