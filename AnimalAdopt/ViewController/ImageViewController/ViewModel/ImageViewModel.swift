//
//  ImageViewModel.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/12.
//

import Foundation
import UIKit.UIImageView

class ImageViewModel{

    var urlString: String
    
    var onImageGet: ((_ image: UIImage?) -> Void)?
    
    private let imageView = UIImageView()
    
    init(urlString: String) {
        self.urlString = urlString
    }
    
    
}


extension ImageViewModel{
    
    
    func getImage(){
        
        guard let imageGet = onImageGet else { return }
        
        if urlString == ""{
            imageGet(nil)
            return
        }else{
            imageView.sd_setImage(with: URL(string: urlString)) { [weak self] (image, err, cacheType, url) in
                guard let _ = self else { return }
                DispatchQueue.main.async {
                    imageGet(image)
                }
            }
        }
    }
    
}

extension ImageViewModel{
    
    func genTapGesture() -> UIGestureRecognizer?{
        if let url = URL(string: urlString),
           UIApplication.shared.canOpenURL(url){
            return UITapGestureRecognizer(target: self, action: #selector(tapAction))
        }
        return nil
    }
    
    @objc private func tapAction(){
        if let navC = UIApplication.shared.visibleViewController as? UINavigationController{
            let vcImageView = ImageViewController(viewModel: self)
            vcImageView.modalPresentationStyle = .popover
            navC.pushViewController(vcImageView, animated: true)
        }else if let navC = UIApplication.shared.visibleViewController?.navigationController{
            let vcImageView = ImageViewController(viewModel: self)
            vcImageView.modalPresentationStyle = .popover
            navC.pushViewController(vcImageView, animated: true)
        }
    }
    
}

