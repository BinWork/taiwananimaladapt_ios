//
//  FavoriteViewController.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/25.
//

import UIKit
class FavoriteViewController: BaseViewController {

    let viewModel = FavoriteViewModel()
    private var isPresented: Bool = false
    
    let tableView: FavoriteListTableView = {
        return FavoriteListTableView(frame: .zero, style: .plain)
    }()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setup()
        isPresented = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addUI()
        
    }

    private func addUI(){
        
        view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            if #available(iOS 11.0, *) {
                make.top.equalTo(view.safeAreaLayoutGuide).offset(snpOffset.top)
            } else {
                make.top.equalToSuperview().offset(40)
            }
            make.left.equalToSuperview().offset(snpOffset.left)
            make.right.equalToSuperview().offset(snpOffset.right)
            make.bottom.equalToSuperview().offset(snpOffset.bottom * 2)
        }
        
    }

}


extension FavoriteViewController{
    
    private func setup(){
        viewModel.loadFavorite()
        tableView.viewModels = viewModel.cellModels
        tableView.baseTableViewViewModel.isReloadAnimated = isPresented ? false : true
        tableView.reloadData()
    }
    
}
