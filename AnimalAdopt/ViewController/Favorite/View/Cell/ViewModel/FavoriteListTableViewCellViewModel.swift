//
//  FavoriteListTableViewCellViewModel.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/27.
//

import Foundation
import UIKit
class FavoriteListTableViewCellViewModel: AnimalAdoptTableViewCellViewModel{
    
}

extension FavoriteListTableViewCellViewModel{
    
    func removeFavorite(_ sender: UIButton){
        if let cell = sender.parentView(of: FavoriteListTableViewCell.self),
           let tableView = cell.parentView(of: FavoriteListTableView.self){
            
            let point = sender.convert(CGPoint.zero, to: tableView)
            guard let indexPath = tableView.indexPathForRow(at: point) else { return }
            DataStoreManager.favorite.remove(index: indexPath.section)
            tableView.viewModels.remove(at: indexPath.section)
            tableView.removeSection(indexPath: indexPath)
        }
        
    }
}
