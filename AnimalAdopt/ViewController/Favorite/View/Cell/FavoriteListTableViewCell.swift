//
//  FavoriteListTableViewCell.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/27.
//

import UIKit

class FavoriteListTableViewCell: BaseTableViewCell {
    
    static var cellID: String = String(describing: self)

    private var viewModel: FavoriteListTableViewCellViewModel?
    
    
    
    private var imageAnimal: AnimalPictureView = {
        let v = AnimalPictureView()
        v.contentMode = .scaleAspectFill
        v.clipsToBounds = true
        v.image = nil
        return v
    }()
    
    private let viewFeature: AnimalFeatureView = {
        let v = AnimalFeatureView()
        return v
    }()
    
    private var stackPlaceFound: UIStackView = {
        let s = UIStackView()
        s.set(Axis: .horizontal, Spacing: 5, Distribution: .fillProportionally)
        return s
    }()
    
    private let imageViewNet: UIImageView = {
        let v = UIImageView()
        v.contentMode = .scaleAspectFit
        return v
    }()
    
    private let lblPlaceFound: UILabel = {
        let lbl = UILabel()
        lbl.lineBreakMode = .byTruncatingTail
        return lbl
    }()
    
    
    
    

    
    private lazy var btnFavorite: BaseButton = {
        let btn = BaseButton()
        btn.setImage(UIImage(named: ImageName.Button.heart_outline), for: .normal)
        btn.setImage(UIImage(named: ImageName.Button.heart), for: .selected)
        return btn
    }()
    
    
    
    
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateUIOnLayoutSubview()
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .defaultBackgroundColor
        
        addUI()
        setStackFoundPlace()
    }
    
    private func addUI(){
        
        contentView.addSubview(imageAnimal)
        imageAnimal.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(snpOffset.left * 2)
            make.height.equalToSuperview().multipliedBy(0.64)
            make.width.equalTo(imageAnimal.snp.height)
            
        }
        
        contentView.addSubview(viewFeature)
        viewFeature.snp.makeConstraints { (make) in
            make.bottom.equalTo(snp.centerY).offset(snpOffset.bottom * 2)
            make.left.equalTo(imageAnimal.snp.right).offset(snpOffset.left * 2)
            make.height.equalTo(20)
        }
        contentView.addSubview(stackPlaceFound)
        stackPlaceFound.snp.makeConstraints { (make) in
            make.top.equalTo(snp.centerY).offset(snpOffset.top * 2)
            make.left.equalTo(imageAnimal.snp.right).offset(snpOffset.left * 2)
            make.height.equalTo(20)
        }
        
        contentView.addSubview(btnFavorite)
        btnFavorite.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(snpOffset.right * 3)
            make.left.equalTo(stackPlaceFound.snp.right).offset(snpOffset.left * 3)
            make.centerY.equalTo(imageAnimal)
            make.height.equalTo(imageAnimal).multipliedBy(0.4375)
            make.width.equalTo(btnFavorite.snp.height).multipliedBy(1.125)
        }
        
    }


    private func updateUIOnLayoutSubview(){
        imageAnimal.makeCornerRound()
        if let model = viewModel{
            imageAnimal.makeBorder(width: .animalImageBorderWidth, color: model.gender.color)
        }else{
            imageAnimal.makeBorder(width: .animalImageBorderWidth, color: .defaultTextColor)
        }
        makeCorner(radius: .defaultCornerWidth)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension FavoriteListTableViewCell{
    
    func setup(viewModel new: FavoriteListTableViewCellViewModel, index: Int){
        
        viewModel = new
        
        guard let _model = viewModel else { return }
        
        imageAnimal.viewModel = _model.imageViewModel
        
        viewFeature.viewModel = .init(
            kind: _model.kind,
            description: _model.color,
            gender: _model.gender
        )
        viewFeature.setup()
        
        imageViewNet.image = UIImage(named: ImageName.Animal.net)
        lblPlaceFound.text = _model.foundPlace
        

        btnFavorite.isSelected = true
        btnFavorite.viewModel.onTouch = { [weak self]  in
            guard let sf = self else { return }
            _model.removeFavorite(sf.btnFavorite)
        }
    }

    
}






extension FavoriteListTableViewCell{
    
    
    private func setStackFoundPlace(){
        stackPlaceFound.addArrangedSubview(imageViewNet)
        imageViewNet.snp.makeConstraints { (make) in
            make.width.height.equalTo(20)
        }
        stackPlaceFound.addArrangedSubview(lblPlaceFound)
    }
    
    
}
