//
//  FavoriteViewModell.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/25.
//

import Foundation

class FavoriteViewModel{
    
    var cellModels: [FavoriteListTableViewCellViewModel] = []
    var onFavoritesChanged: (() -> ())?
}

extension FavoriteViewModel{
    
    func loadFavorite(){
        cellModels.removeAll()
        cellModels = convertToCellModels(with: DataStoreManager.favorite.favorites)
    }
    
    private func convertToCellModels(with data: [Animal.FavoriteModel]) -> [FavoriteListTableViewCellViewModel]{
        var arr: [FavoriteListTableViewCellViewModel] = []
        for d in data{
            let _cellModel = FavoriteListTableViewCellViewModel(model: d)
            arr.append(_cellModel)
        }
        return arr
    }
}
