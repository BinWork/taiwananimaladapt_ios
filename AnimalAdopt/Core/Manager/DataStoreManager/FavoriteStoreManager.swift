//
//  FavoriteStoreManager.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/27.
//

import Foundation

class FavoriteStoreManager{
    
    var favorites: [Animal.FavoriteModel] = []{
        didSet{
            NotificationCenter.default.post(name: NotificationName.Favorites.didChanged, object: self, userInfo: [NotificationName.Favorites.key: favorites])
        }
    }
    
    init() {
        loadFromLocal()
    }
    
}

extension FavoriteStoreManager{
    
    func save(with viewModel: AnimalAdoptDetailViewModel){
        let _viewModel = Animal.FavoriteModel.init(viewModel)
        if isContain(with: _viewModel) == false{
            favorites.append(_viewModel)
        }
    }

    
}




extension FavoriteStoreManager{
    
    func remove(with viewModel: AnimalAdoptDetailViewModel){
        let _viewModel = Animal.FavoriteModel.init(viewModel)
        favorites = favorites.filter({ $0 != _viewModel })
    }

    func remove(index: Int){
        favorites.remove(at: index)
    }

}



extension FavoriteStoreManager{
    
    func loadFromLocal(){
        if let data = UserDefaults.standard.object(forKey: Animal.FavoriteModel.key) as? Data {
            let decoder = PropertyListDecoder()
            if let favorite = try? decoder.decode(Array<Animal.FavoriteModel>.self, from: data){
                favorites = favorite
            }
        }
    }
}





extension FavoriteStoreManager{
    
    func updateLocal(){
        let userDefault = UserDefaults.standard
        if let favoriteData: Data = try? PropertyListEncoder().encode(favorites){
            userDefault.removeObject(forKey: Animal.FavoriteModel.key)
            userDefault.set(favoriteData, forKey: Animal.FavoriteModel.key)
        }
    }
    
}






extension FavoriteStoreManager{
    
    func isContain(with viewModel: Animal.FavoriteModel) -> Bool{
        return favorites.contains(where: {$0 == viewModel})
    }
    
    func isContain(with model: AnimalAdoptTableViewCellViewModel) -> Bool{
        if favorites.filter({ $0.id == model.id.toString }).count > 0{
            return true
        }
        return false
    }
    
}



