//
//  ErrorManager.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/25.
//

import Foundation
import UIKit


enum RequestError: Error{
    case InValidURL(String)
    case ConnectionProblem(String)
    case ParsingFailed
    case RequestError(Error)
    case ResponseIsNil
    case DataIsNil
    
    var content: String{
        switch self{
        case .InValidURL(let url):
            return url
        case .ConnectionProblem(let url):
            return url
        case .ParsingFailed:
            return "Parsing data failed."
        case .RequestError(let err):
            return err.localizedDescription
        case .ResponseIsNil:
            return "Request's response is nil."
        case .DataIsNil:
            return "Request's data is nil."
        }
    }
}



class ErrorManager{
    
    static let shared = ErrorManager()
    
    func handle(_ error: Error?){
        guard let _error = error else { return }
        switch _error{
        case let err as RequestError:
            popAlert(title: "Request Error", content: Localizations.System.tryAgain)
            track(err.content)
            
        default:
            popAlert()
            track(_error.localizedDescription)
        }
    }
    
}







extension ErrorManager{
    
    private func popAlert(title: String? = nil, content: String? = nil){
        DispatchQueue.main.async {
            if let vc = UIApplication.shared.visibleViewController,
               vc.isKind(of: UIAlertController.self) == false{
                let alert = UIAlertController(title: title, message: content, preferredStyle: .alert)
                let cancel = UIAlertAction(title: "確定", style: .default, handler: nil)
                alert.addAction(cancel)
                vc.present(alert, animated: true, completion: nil)
            }else{
                track("找不到visibleController")
            }
        }
    }
    
}


