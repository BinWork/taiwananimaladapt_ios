//
//  NotificationName.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/27.
//

import Foundation

enum NotificationName{
    
    enum Favorites{
        static let key = "Favorites"
        static let didChanged = Notification.Name("NotificationName.Favorites.didChanged")
    }
    
    enum Search{
        static let key = "Search"
        static let didConditionChanged = Notification.Name("NotificationName.Search.didConditionChanged")
    }
}
