//
//  APIManager.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/08/17.
//

import Foundation

protocol APIManagerRequestProccessProtocol: AnyObject{
    func add(requestModel: RequestModel)
    func removeAll()
}

protocol IndicatorDisplayProccessProtocol: AnyObject{
    func showIndicator()
    func hideIndicator()
}

class APIManager{
    static let domain: String = "https://data.coa.gov.tw/Service/OpenData/TransService.aspx"
    private var requestWaitingBeProccess: [RequestModel] = []
    private var isRequestProccessing: Bool = false
}

extension APIManager: APIManagerRequestProccessProtocol{
    func add(requestModel: RequestModel) {
        requestWaitingBeProccess.append(requestModel)
    }

    func removeAll(){
        requestWaitingBeProccess.removeAll()
    }
}

extension APIManager{

    func getAnimalAdoptList(
        request: AdoptRequest,
        completion: @escaping (Result< [Animal.RecognitionModel], RequestError>) -> Void){
        call(requestModel: request, response: [Animal.RecognitionModel].self) {
            completion($0)
        }
    }

    func getPetLostList(
        request: PetLoseRequest,
        completion: @escaping (Result< [Animal.MissingModel], RequestError>) -> Void){
        call(requestModel: request, response: [Animal.MissingModel].self) {
            completion($0)
        }
    }
}

extension APIManager{

    private func call<T: Decodable>(
        requestModel: RequestModel, response: T.Type, completion:
        @escaping (Result< T, RequestError>) -> Void) {

        enterRequestPhase()

        guard let request = requestModel.request else { return }
        sendRequest(with: request, response: response) { [weak self] (result) in
            guard let sf = self else { return }
            switch result{
            case .success(let s):
                completion(.success(s))
            case .failure(let err):
                completion(.failure(err))
            }
            sf.exitRequestPhase()
        }
    }

    private func sendRequest<T: Decodable>(
        with request: URLRequest, response: T.Type , completion:
        @escaping (Result< T, RequestError>) -> Void){

        let task = URLSession.shared.dataTask(with: request) {
            (requestData, requestRes, requestErr) in
            if let err =  requestErr{
                completion(.failure(.RequestError(err)))
                return
            }
            guard let data = requestData else{
                completion(.failure(.DataIsNil))
                return
            }
            do{
                let json = try JSONDecoder().decode(response.self, from: data)
                completion(.success(json))
            }catch{
                completion(.failure(.ParsingFailed))
            }
        }
        task.resume()
    }

}

extension APIManager{

    func enterRequestPhase(){
        isRequestProccessing = true
        showIndicator()
    }

    func exitRequestPhase(){
        isRequestProccessing = false
        hideIndicator()
    }

}

//  MARK: - IndicatorDisplayProccessProtocol
extension APIManager: IndicatorDisplayProccessProtocol{
    
    func showIndicator() {

    }
    func hideIndicator() {

    }
}
