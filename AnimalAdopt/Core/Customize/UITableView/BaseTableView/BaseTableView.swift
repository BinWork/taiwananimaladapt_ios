//
//  BaseTableView.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/06.
//

import UIKit


class BaseTableView: UITableView {

    lazy var baseTableViewViewModel = BaseTableViewViewModel(tableView: self)
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        separatorStyle = .none
        backgroundColor = .clear
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func reloadData() {
        super.reloadData()
        baseTableViewViewModel.reloadDataAnimatedIfNeed()
    }
    
}

extension BaseTableView{
    
    func removeSection(indexPath: IndexPath, animation: UITableView.RowAnimation = .automatic){
        beginUpdates()
        deleteSections(.init(integer: indexPath.section), with: animation)
        endUpdates()
    }
    
}


extension BaseTableView{
    
    func removeRow(indexPath: IndexPath, animation: UITableView.RowAnimation = .automatic){
        removeRows(indexPaths: [indexPath], animation: animation)
    }
    
    func removeRows(indexPaths: [IndexPath], animation: UITableView.RowAnimation = .automatic){
        beginUpdates()
        deleteRows(at: indexPaths, with: animation)
        endUpdates()
    }
    
}
