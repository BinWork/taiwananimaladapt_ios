//
//  BaseTableViewViewModel.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/13.
//

import UIKit
import CRRefresh

class BaseTableViewViewModel{
    
    weak var tableView: UITableView?
    /// default value: true
    var isReloadAnimated: Bool = true
    
    
    var onHeaderPullRefresh: (()->Void)?
    var isHeaderRefresh: Bool = false{
        didSet{
            configHeaderRefresh()
        }
    }
    
    var onFooterPullRefresh: (()->Void)?
    var isFooterRefresh: Bool = false{
        didSet{
            configFooterRefresh()
        }
    }
    
    
    init(tableView: UITableView) {
        self.tableView = tableView
    }
    

}

extension BaseTableViewViewModel: TableViewProtocol{
    
    func setProperties(dataSource ds: UITableViewDataSource?, delegate dg: UITableViewDelegate?, cellClass: AnyClass?, cellReuseId: String?) {
        
        tableView?.dataSource = ds
        tableView?.delegate = dg
        guard let cs = cellClass,
              let id = cellReuseId else { return }
        tableView?.register(cs, forCellReuseIdentifier: id)
    }
    
}

extension BaseTableViewViewModel{
    
    func reloadDataAnimatedIfNeed(){
        
        guard let tb = self.tableView else { return }
        
        guard isReloadAnimated else { return }
        
        let AnimationDuration:TimeInterval = 1.0
        
        let cells = tb.visibleCells
        let tableHeight: CGFloat = tb.bounds.size.height
        
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            a.isUserInteractionEnabled = false
            tb.isHidden = false
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: AnimationDuration, delay: 0.04 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .transitionCrossDissolve, animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: { finish in
                a.isUserInteractionEnabled = true
            })
            
            index += 1
        }
    }
}


extension BaseTableViewViewModel{
    
    private func configHeaderRefresh(){
        if isHeaderRefresh{
            addHeaderRefresh()
        }else{
            removeHeaderRefresh()
        }
    }
    
    private func addHeaderRefresh(){
        tableView?.cr.addHeadRefresh { [weak self] in
            guard let sf = self else { return }
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                if let pullRefresh = sf.onHeaderPullRefresh{
                    pullRefresh()
                }
                sf.tableView?.cr.endHeaderRefresh()
            }
        }
    }
    
    private func removeHeaderRefresh(){
        tableView?.cr.removeHeader()
    }
    
}


extension BaseTableViewViewModel{
    
    private func configFooterRefresh(){
        if isFooterRefresh{
            addFooterRefresh()
        }else{
            removeFooterRefresh()
        }
    }
    
    private func addFooterRefresh(){
        tableView?.cr.addFootRefresh { [weak self] in
            guard let sf = self else { return }
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                if let pullRefresh = sf.onFooterPullRefresh{
                    pullRefresh()
                }
                sf.tableView?.cr.endLoadingMore()
            }
        }
    }
    
    private func removeFooterRefresh(){
        tableView?.cr.removeFooter()
    }
    
}
