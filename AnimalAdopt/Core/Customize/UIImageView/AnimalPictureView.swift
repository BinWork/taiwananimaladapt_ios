//
//  AnimalPictureView.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/08.
//

import UIKit

class AnimalPictureView: UIImageView {
    
    var viewModel: ImageViewModel?{
        didSet{
            setup()
        }
    }
    
    override var image: UIImage?{
        didSet{
            addUI()
        }
    }
    
    
    let labelNoImage: UILabel = {
        let lbl = UILabel()
        lbl.text = Localizations.System.Animal.imageNotFound
        lbl.font = .systemFont(ofSize: 20)
        lbl.adjustsFontSizeToFitWidth = true
        lbl.textAlignment = .center
        lbl.baselineAdjustment = .alignCenters
        lbl.minimumScaleFactor = 0.5
        lbl.textColor = UIColor.defaultTextColor
        return lbl
    }()
    
}


extension AnimalPictureView{
    
    private func addUI(){
        if image == nil{
            addSubview(labelNoImage)
            labelNoImage.snp.makeConstraints { (make) in
                make.center.equalToSuperview()
                make.width.equalToSuperview().multipliedBy(0.7)
                make.height.equalToSuperview().multipliedBy(0.4)
            }
        }else{
            labelNoImage.removeFromSuperview()
        }
        
    }
    
}

extension AnimalPictureView{
    
    func setup(){
        viewModel?.onImageGet = { [weak self] (img) in
            guard let sf = self else { return }
            DispatchQueue.main.async {
                sf.image = img
            }
        }
        viewModel?.getImage()
        
        
        
        if let gesture = viewModel?.genTapGesture(){
            isUserInteractionEnabled = true
            addGestureRecognizer(gesture)
        }else{
            gestureRecognizers?.forEach({ (gesture) in
                removeGestureRecognizer(gesture)
            })
        }
    }
    
}
