//
//  AnimalStatusViewViewModel.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/19.
//

import Foundation
import UIKit.UIColor

class AnimalStatusViewViewModel{
    
    var gender: Animal.Gender
    
    var statusAvailable: Animal.Status
    var sterilization: Animal.Boolean
    var bacterin: Animal.Boolean
    
    var colorStatusAvailable_Background: UIColor?
    var colorSterilization_Background: UIColor?
    var colorBacterin_Background: UIColor?
    
    var colorStatusAvailable_Text: UIColor?
    var colorSterilization_Text: UIColor?
    var colorBacterin_Text: UIColor?
    
    init(viewModel: AnimalAdoptDetailViewModel){
        self.gender = viewModel.gender
        
        self.statusAvailable = viewModel.statusAvailable
        self.sterilization = viewModel.sterilization
        self.bacterin = viewModel.bacterin
        setColor()
    }
    
    private func setColor(){
        
        switch self.statusAvailable {
        case .open:
            colorStatusAvailable_Background = .animalStatus_On
            colorStatusAvailable_Text = .animalStatusTextColor_On
        default:
            colorStatusAvailable_Background = .animalStatus_Off
            colorStatusAvailable_Text = .animalStatusTextColor_Off
        }
        
        switch self.sterilization {
        case .yes:
            colorSterilization_Background = .animalStatus_On
            colorSterilization_Text = .animalStatusTextColor_On
        default:
            colorSterilization_Background = .animalStatus_Off
            colorSterilization_Text = .animalStatusTextColor_Off
        }
        
        switch self.bacterin {
        case .yes:
            colorBacterin_Background = .animalStatus_On
            colorBacterin_Text = .animalStatusTextColor_On
        default:
            colorBacterin_Background = .animalStatus_Off
            colorBacterin_Text = .animalStatusTextColor_Off
        }
        
    }
}
