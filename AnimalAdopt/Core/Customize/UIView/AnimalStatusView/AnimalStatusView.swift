//
//  AnimalStatusView.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/19.
//

import UIKit


/// 顯示動物的一些狀態(開放認養的狀態、結紮、狂犬病疫苗狀態)
class AnimalStatusView: UIView {

    var viewModel: AnimalStatusViewViewModel?
    
    private let stackView: UIStackView = {
        let v = UIStackView()
        v.set(Axis: .horizontal, Spacing: 5, Distribution: .fillEqually)
        return v
    }()
    
    private let lblStatusAvailable: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.minimumScaleFactor = 0.4
        return lbl
    }()
    
    private let lblSterilization: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.minimumScaleFactor = 0.4
        return lbl
    }()
    
    private let lblBacterin: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.minimumScaleFactor = 0.4
        return lbl
    }()
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        lblStatusAvailable.makeCorner(radius: 5)
        lblSterilization.makeCorner(radius: 5)
        lblBacterin.makeCorner(radius: 5)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
 
    private func addUI(){
        addSubview(stackView)
        stackView.snp.makeConstraints { (make) in
            make.top.left.right.bottom.equalToSuperview()
        }
        
        stackView.addArrangedSubview(lblStatusAvailable)
        stackView.addArrangedSubview(lblSterilization)
        stackView.addArrangedSubview(lblBacterin)
        
    }
    
}


extension AnimalStatusView{
    
    func setup(){
        guard let _viewModel = viewModel else { return }
        
        lblStatusAvailable.text = _viewModel.statusAvailable.string.represent
        lblSterilization.text = {
            switch _viewModel.gender{
            case .male, .male_zh,
                 .NA:
                return Localizations.Animal.Data.Sterilization.male
            case .female, .female_zh:
                return Localizations.Animal.Data.Sterilization.female
            }
        }()
        lblBacterin.text = Localizations.Animal.Data.Status.rabiesVaccine
        
        lblStatusAvailable.backgroundColor = _viewModel.colorStatusAvailable_Background
        lblSterilization.backgroundColor = _viewModel.colorSterilization_Background
        lblBacterin.backgroundColor = _viewModel.colorBacterin_Background
        
        lblStatusAvailable.textColor = _viewModel.colorStatusAvailable_Text
        lblSterilization.textColor = _viewModel.colorSterilization_Text
        lblBacterin.textColor = _viewModel.colorBacterin_Text
    }
    
}
