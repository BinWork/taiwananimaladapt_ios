//
//  AnimalInfoViewViewModel.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/19.
//


import Foundation
import UIKit.UIColor

class AnimalInfoViewViewModel{
    
    var bodyType: Animal.BodyType
    
    init(viewModel: AnimalAdoptDetailViewModel){
        self.bodyType = viewModel.bodyType
    }
    
}
