//
//  AnimalFeatureViewViewModel.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/17.
//

import Foundation
import UIKit.UIImage

class AnimalFeatureViewViewModel{
    
    var kind: Animal.Kind
    var description: String?
    var gender: Animal.Gender
    
    init(kind: Animal.Kind, description: String?, gender: Animal.Gender) {
        self.kind = kind
        self.description = description
        self.gender = gender
    }
}

