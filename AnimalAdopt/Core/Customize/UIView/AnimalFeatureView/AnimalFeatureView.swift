//
//  AnimalFeatureView.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/17.
//

import UIKit


/// 顯示動物類別、毛色與種類
class AnimalFeatureView: UIView {

    var viewModel: AnimalFeatureViewViewModel?
    
    private var stackFeature: UIStackView = {
        let s = UIStackView()
        s.set(Axis: .horizontal, Spacing: 5, Distribution: .equalSpacing)
        s.sizeToFit()
        s.alignment = .leading
        return s
    }()
    
    private let imageViewKind: UIImageView = {
        let v = UIImageView()
        v.contentMode = .scaleAspectFit
        return v
    }()
    
    private let lblDescription: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.font = .systemFont(ofSize: 18)
        return lbl
    }()
    
    private let imageViewGender: UIImageView = {
        let v = UIImageView()
        v.contentMode = .scaleAspectFit
        return v
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        addUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


extension AnimalFeatureView{
    
    private func addUI(){
        addSubview(stackFeature)
        stackFeature.snp.makeConstraints { (make) in
            make.top.left.right.bottom.equalToSuperview()
        }
        
        stackFeature.addArrangedSubview(imageViewKind)
        imageViewKind.snp.makeConstraints { (make) in
            make.width.height.equalTo(20)
        }
        stackFeature.addArrangedSubview(lblDescription)
        stackFeature.addArrangedSubview(imageViewGender)
        imageViewGender.snp.makeConstraints { (make) in
            make.width.height.equalTo(20)
        }
    }
}


extension AnimalFeatureView{
    
    func setup(){
        
        guard let _viewModel = viewModel else { return }
        
        imageViewKind.image = _viewModel.kind.icon
        lblDescription.text = _viewModel.description
        imageViewGender.image = _viewModel.gender.icon
        
    }
}
