//
//  AnimalDetailedViewViewModel.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/20.
//

import Foundation
import UIKit
class AnimalDetailedViewViewModel{
    
    var animalViewModel: Any?
    
    let tagButtons: Int = 123
    
    enum SectionCase{
        case chip
        case feature
        case kind
        case bodyType
        case age
        case foundPlace
        case lostPlace
        case remark
        case lostDate
        case shelterName
        case shelterAddress
        case openDate
        case shelterTel
        case ownerName
        case ownerTel
        case ownerEmail
        
        var title: String{
            switch self{
            case .chip:
                return Localizations.Animal.Data.chip + ":"
            case .feature:
                return Localizations.Animal.Data.feature + ":"
            case .kind:
                return Localizations.Animal.Data.Kind.title + ":"
            case .bodyType:
                return Localizations.Animal.Data.BodyType.title + ":"
            case .age:
                return Localizations.Animal.Data.Age.title + ":"
            case .foundPlace:
                return Localizations.Animal.Data.foundPlace + ":"
            case .remark:
                return Localizations.Animal.Data.remark + ":"
            case .lostDate:
                return Localizations.Animal.Data.lostDate + ":"
            case .lostPlace:
                return Localizations.Animal.Data.lostPlace + ":"
            case .shelterName:
                return Localizations.Animal.Data.Shelter.name + ":"
            case .shelterAddress:
                return Localizations.Animal.Data.Shelter.address + ":"
            case .openDate:
                return Localizations.Animal.Data.openDate + ":"
            case .shelterTel:
                return Localizations.Animal.Data.Shelter.telephone + ":"
            case .ownerName:
                return Localizations.Animal.Data.Owner.name + ":"
            case .ownerTel:
                return Localizations.Animal.Data.Owner.telephone + ":"
            case .ownerEmail:
                return Localizations.Animal.Data.Owner.email + ":"
            }
        }
        
        var icon: UIImage?{
            switch self{
            case .chip:
                return UIImage(named: ImageName.Animal.chip)
            case .feature:
                return UIImage(named: ImageName.Animal.body)
            case .kind:
                return UIImage(named: ImageName.Animal.breed)
            case .bodyType:
                return UIImage(named: ImageName.Animal.body)
            case .age:
                return UIImage(named: ImageName.Animal.age)
            case .foundPlace:
                return UIImage(named: ImageName.Animal.net)
            case .lostPlace:
                return UIImage(named: ImageName.Animal.missing_location)
            case .remark:
                return UIImage(named: ImageName.Animal.note)
            case .lostDate:
                return UIImage(named: ImageName.Animal.date)
            case .shelterName:
                return UIImage(named: ImageName.Animal.shelter)
            case .shelterAddress:
                return UIImage(named: ImageName.Animal.pin)
            case .openDate:
                return UIImage(named: ImageName.Animal.calendar)
            case .shelterTel:
                return UIImage(named: ImageName.Animal.phone)
            case .ownerName:
                return UIImage(named: ImageName.Animal.shelter)
            case .ownerTel:
                return UIImage(named: ImageName.Animal.phone)
            case .ownerEmail:
                return UIImage(named: ImageName.Animal.email)
            }
        }
    }
    
    
}

extension AnimalDetailedViewViewModel{
    
    func getArrangeView(with sections: [SectionCase]) -> [UIView]{
        var _views: [UIView] = []
        
        for section in sections{
            if let _stackLabelView = generateLabel(with: section){
                _views.append(_stackLabelView)
            }
        }
        
        if sections.contains(.shelterTel) ||
            sections.contains(.ownerTel)  ||
            sections.contains(.ownerEmail){
            if let _stackButtonView = generateButton(with: sections){
                _views.append(_stackButtonView)
            }
        }

        return _views
    }

    
}






extension AnimalDetailedViewViewModel{
    
    private func generateLabel(with section: SectionCase) -> UIStackView?{
        guard let _animalViewModel = animalViewModel else { return nil}
        let _imageView = UIImageView(image: section.icon)
        _imageView.contentMode = .scaleAspectFit
        
        let _labelTitle = UILabel()
        _labelTitle.textColor = .peacock
        _labelTitle.text = section.title
        _labelTitle.sizeToFit()
        _labelTitle.numberOfLines = 0
        
        let _labelContent = UILabel()
        _labelContent.textColor = .defaultTextColor
        _labelContent.minimumScaleFactor = 0.6
        _labelContent.adjustsFontSizeToFitWidth = true
        _labelContent.numberOfLines = 0
        
        if let _viewModel = _animalViewModel as? AnimalAdoptDetailViewModel{
            switch section{
            case .bodyType:
                _labelContent.text = _viewModel.bodyType.string.represent
            case .age:
                _labelContent.text = _viewModel.age.string.represent
            case .foundPlace:
                _labelContent.text = _viewModel.foundPlace.represent
            case .remark:
                _labelContent.text = _viewModel.remark.represent
            case .shelterName:
                _labelContent.text = _viewModel.shelterName
            case .shelterAddress:
                _labelContent.text = _viewModel.shelterAddress
            case .openDate:
                _labelContent.text = _viewModel.openDate
            case .shelterTel:
                _labelContent.text = _viewModel.shelterTel
            default:
                break
            }
        }else if let _viewModel = _animalViewModel as? AnimalFoundDetailViewModel{
            
            switch section{
            case .chip:
                _labelContent.text = _viewModel.chip.represent
            case .feature:
                _labelContent.text = _viewModel.feature.represent
            case .kind:
                _labelContent.text = _viewModel.kind.string.represent
            case .lostPlace:
                _labelContent.text = _viewModel.lostPlace.represent
            case .lostDate:
                _labelContent.text = _viewModel.lostTime.represent
            case .ownerName:
                _labelContent.text = _viewModel.ownerName.represent
            case .ownerTel:
                _labelContent.text = _viewModel.ownerTel.represent
            case .ownerEmail:
                _labelContent.text = _viewModel.ownerEmail.represent
            default:
                break
            }
        }
        let _stackView = UIStackView(arrangedSubviews: [_imageView, _labelTitle, _labelContent])
        _stackView.set(Axis: .horizontal, Spacing: 10, Distribution: .fill)
        _stackView.alignment = .center
        _stackView.sizeToFit()
        
        _imageView.snp.makeConstraints { (make) in
            make.height.equalTo(20)
            make.width.equalTo(_imageView.snp.height).multipliedBy(2)
        }
        
        _labelTitle.snp.makeConstraints { (make) in
            make.width.equalToSuperview().multipliedBy(0.216)
        }
        
        return _stackView
    }
    
    
}
















extension AnimalDetailedViewViewModel{
    
    private func generateButton(with sections: [SectionCase]) -> UIStackView?{
        
        guard let _animalViewModel = animalViewModel else { return nil}
        
        let _stackView = UIStackView()
        _stackView.set(Axis: .horizontal, Spacing: 5, Distribution: .fillEqually)
        _stackView.alignment = .fill
        _stackView.isUserInteractionEnabled = true
        _stackView.sizeToFit()
        _stackView.tag = tagButtons
        
        for section in sections{
            switch section{
            case .shelterTel, .ownerTel, .ownerEmail:
                
                let _button = BaseButton()
                switch section {
                case .shelterTel:
                    if let _viewModel = _animalViewModel as? AnimalAdoptDetailViewModel{
                        _button.setImage(UIImage(named: ImageName.Button.telephone), for: .normal)
                        _button.addAction { [weak self] in
                            guard let sf = self else { return }
                            sf.makePhoneCall(number: _viewModel.shelterTel)
                        }
                    }
                case .ownerTel:
                    if let _viewModel = _animalViewModel as? AnimalFoundDetailViewModel{
                        _button.setImage(UIImage(named: ImageName.Button.telephone), for: .normal)
                        _button.addAction { [weak self] in
                            guard let sf = self else { return }
                            sf.makePhoneCall(number: _viewModel.ownerTel)
                        }
                    }
                case .ownerEmail:
                    if let _viewModel = _animalViewModel as? AnimalFoundDetailViewModel{
                        _button.setImage(UIImage(named: ImageName.Button.email), for: .normal)
                        _button.addAction { [weak self] in
                            guard let sf = self else { return }
                            sf.sendEmail(email: _viewModel.ownerEmail)
                        }
                    }
                default:
                    break
                }
                _stackView.addArrangedSubview(_button)
            default:
                break
            }
        }
        return _stackView.arrangedSubviews.count > 0 ? _stackView : nil
    }
    
    
    @objc private func makePhoneCall(number: String){
        let _number = number
        guard let urlPhoneCall = URL(string: "tel://\(_number)") else { return }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(urlPhoneCall, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(urlPhoneCall)
        }
    }
    
    @objc private func sendEmail(email: String){
        
        guard let urlMailTo = URL(string: "mailto:\(email)") else { return }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(urlMailTo, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(urlMailTo)
        }
        
    }
    
}

