//
//  AnimalDetailedView.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/21.
//

import UIKit

class AnimalDetailedView: UIView {

    let viewModel = AnimalDetailedViewViewModel()
    
    private let stackView: UIStackView = {
        let v = UIStackView()
        v.set(Axis: .vertical, Spacing: 5, Distribution: .fill)
        v.sizeToFit()
        return v
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(sections: [AnimalDetailedViewViewModel.SectionCase]){
        let arrangeViews = viewModel.getArrangeView(with: sections)
        for arrangedView in arrangeViews{
            stackView.addArrangedSubview(arrangedView)
            arrangedView.snp.makeConstraints { (make) in
                if arrangedView.tag == viewModel.tagButtons{
                    make.height.greaterThanOrEqualTo(60)
                    make.height.equalTo(60).priority(900)
                }else{
                    make.height.greaterThanOrEqualTo(45)
                    make.height.equalTo(45).priority(900)
                }
            }
        }
    }
    
}

extension AnimalDetailedView{
    
    private func addUI(){
        addSubview(stackView)
        stackView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(snpOffset.top)
            make.left.equalToSuperview().offset(snpOffset.left)
            make.right.equalToSuperview().offset(snpOffset.right)
            make.bottom.equalToSuperview().offset(snpOffset.bottom)
        }
    }
    
}
