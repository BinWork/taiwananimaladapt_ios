//
//  BaseAnimalDetailViewController.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/13.
//

import UIKit

class BaseAnimalDetailViewController: BaseViewController {
    
    let scrollView: UIScrollView = {
        let v = UIScrollView()
        return v
    }()
    
    let lblDate: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .middleGray
        lbl.font = .systemFont(ofSize: 13)
        return lbl
    }()
    
    let imageView: AnimalPictureView = {
        let v = AnimalPictureView()
        v.contentMode = .scaleAspectFill
        return v
    }()
    
    let viewFeature: AnimalFeatureView = {
        let v = AnimalFeatureView()
        return v
    }()
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imageView.makeCornerRound()
        imageView.makeBorder(width: .animalImageBorderWidth, color: .defaultTextColor)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addUI()
    }
    

    private func addUI(){
        
        view.addSubview(scrollView)
        scrollView.snp.makeConstraints { (make) in
            if #available(iOS 11.0, *) {
                make.top.equalTo(view.safeAreaLayoutGuide)
            } else {
                make.top.equalTo(navigationBarHeight)
            }
            make.bottom.equalToSuperview()
            make.width.equalToSuperview()
        }
        
        scrollView.addSubview(lblDate)
        lblDate.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(snpOffset.top * 3)
            make.right.equalTo(view).offset(snpOffset.right * 2)
            make.height.equalTo(15)
        }
        
        scrollView.addSubview(imageView)
        imageView.snp.makeConstraints { (make) in
            make.top.equalTo(lblDate.snp.bottom).offset(snpOffset.top * 3)
            make.centerX.equalTo(view)
            make.width.equalTo(view).multipliedBy(0.32)
            make.height.equalTo(imageView.snp.width)
        }
        
        scrollView.addSubview(viewFeature)
        viewFeature.snp.makeConstraints { (make) in
            make.top.equalTo(imageView.snp.bottom).offset(snpOffset.top * 2)
            make.centerX.equalToSuperview()
            make.height.equalTo(20)
        }
        
    }

}
