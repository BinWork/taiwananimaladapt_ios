//
//  BaseViewController.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/05.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .lime
        setNavigationBackButton()
    }
    

    private func setNavigationBackButton(){
        let imageV = UIImageView(image: UIImage(named: ImageName.Button.back)?.withRenderingMode(.alwaysOriginal))
        imageV.frame = CGRect(origin: .zero, size: CGSize(width: 30, height: 30))
        imageV.contentMode = .scaleAspectFit
        let back = UIBarButtonItem(customView: imageV)
        self.navigationItem.backBarButtonItem = back
    }
    

    
    func addNavigationRightItems(button: UIButton, tintColor: UIColor? = .peacock){
        let _button = UIBarButtonItem(customView: button)
        navigationItem.rightBarButtonItem = _button
        navigationItem.rightBarButtonItem?.tintColor = tintColor
    }

}
