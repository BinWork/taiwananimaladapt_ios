//
//  BaseButtonViewModel.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/16.
//

import Foundation
import UIKit.UIButton

class BaseButtonViewModel{
    
    var onTouch: (()->())?
    
}


extension BaseButtonViewModel{
    
    @objc func didTouch(){
        if let _onTouch = onTouch{
            _onTouch()
        }
    }
    
}
