//
//  BaseButton.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/16.
//

import UIKit

class BaseButton: UIButton {

    let viewModel = BaseButtonViewModel()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup(){
//        addTarget(viewModel, action: #selector(viewModel.didTouch), for: .touchUpInside)
        addAction { [weak self] in
            guard let sf = self else { return }
            sf.viewModel.didTouch()
        }
    }
    
}
