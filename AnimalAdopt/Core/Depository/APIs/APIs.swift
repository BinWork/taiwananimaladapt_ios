//
//  APIs.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/05.
//

import Foundation


enum APIs: CaseIterable{
    case animalRecognition
    case petLoseList
    
    var queryKey: String{
        return "UnitId"
    }
    
    var queryValue: String{
        switch self {
        case .animalRecognition:
            return "QcbUEzN6E6DL"
        case .petLoseList:
            return "IFJomqVzyB0i"
        @unknown default:
            return ""
        }
    }
    
    
}
