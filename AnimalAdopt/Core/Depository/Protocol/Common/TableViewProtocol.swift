//
//  TableViewProtocol.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/07.
//

import Foundation
import UIKit.UITableView
protocol TableViewProtocol: AnyObject {
    /// 初始化TableView
    /// - Parameters:
    ///   - dataSource: 資料源
    ///   - delegate: 代理
    ///   - registerClass: 註冊的cell class
    ///   - cellReuseId: 註冊的 cell 的 cellID
    func setProperties(dataSource ds: UITableViewDataSource?, delegate dg: UITableViewDelegate?, cellClass: AnyClass?, cellReuseId: String?)
    
}

