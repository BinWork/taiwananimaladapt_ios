//
//  SearchViewControllerDelegate.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/08/15.
//

import Foundation

protocol SearchViewControllerDelegate: AnyObject{

    func search(object: Animal.SearchModel)

}
