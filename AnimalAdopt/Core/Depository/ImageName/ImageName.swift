//
//  ImagePath.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/28.
//

import Foundation

enum ImageName{
    
    enum TabBar{
        static let adopt = "adopt"
        static let favorite = "favorite"
        static let home = "home"
        static let missing = "missing"
    }
    
    enum Animal{
        static let age = "age"
        static let body = "body"
        static let breed = "breed"
        static let calendar = "calendar"
        static let chip = "chip"
        static let date = "date"
        static let email = "email"
        static let missing_location = "missing-location"
        static let net = "net"
        static let note = "note"
        static let phone = "phone"
        static let pin = "pin"
        static let shelter = "shelter"
        
        enum Gender{
            static let male = "male"
            static let female = "female"
            static let unknown = "unknown"
        }
        
        enum Category{
            static let dog = "animalCategoryDog"
            static let cat = "animalCategoryCat"
            static let other = "animalCategoryOther"
        }
        
        
    }
    
    enum Button{
        static let back = "back"
        static let email = "button-email"
        static let cross = "cross"
        static let heart = "heart"
        static let heart_outline = "heart-outline"
        static let search = "search"
        static let telephone = "button-phone"
        static let setting = "setting"
        static let clear = "clear"
    }
}
