//
//  AreaPkId.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/08/02.
//

import Foundation

/// (寵物認養) 動物所屬縣市代碼
enum AreaPkId: String, CaseIterable, Encodable{
    case Taipei_City = "2"
    case NewTaipei_City = "3"
    case Keelung_City = "4"
    case Yilan_County = "5"
    case Taoyuan_City = "6"
    case Hsinchu_County = "7"
    case Hsinchu_City = "8"
    case Miaoli_County = "9"
    case Taichung_City = "10"
    case Changhua_County = "11"
    case Nantou_County = "12"
    case Yunlin_County = "13"
    case Chiayi_County = "14"
    case Chiayi_City = "15"
    case Tainan_City = "16"
    case Kaohsiung_City = "17"
    case Pingtung_County = "18"
    case Hualien_County = "19"
    case Taitung_County = "20"
    case Penghu_County = "21"
    case Kinmen_County = "22"
    case Lienchiang_County = "23"
    
    
    var string: String{
        switch self {
        case .Taipei_City:
            return Localizations.Area.Taipei_City
        case .NewTaipei_City:
            return Localizations.Area.NewTaipei_City
        case .Keelung_City:
            return Localizations.Area.Keelung_City
        case .Yilan_County:
            return Localizations.Area.Yilan_County
        case .Taoyuan_City:
            return Localizations.Area.Taoyuan_City
        case .Hsinchu_County:
            return Localizations.Area.Hsinchu_County
        case .Hsinchu_City:
            return Localizations.Area.Hsinchu_City
        case .Miaoli_County:
            return Localizations.Area.Miaoli_County
        case .Taichung_City:
            return Localizations.Area.Taichung_City
        case .Changhua_County:
            return Localizations.Area.Changhua_County
        case .Nantou_County:
            return Localizations.Area.Nantou_County
        case .Yunlin_County:
            return Localizations.Area.Yunlin_County
        case .Chiayi_County:
            return Localizations.Area.Chiayi_County
        case .Chiayi_City:
            return Localizations.Area.Chiayi_City
        case .Tainan_City:
            return Localizations.Area.Tainan_City
        case .Kaohsiung_City:
            return Localizations.Area.Kaohsiung_City
        case .Pingtung_County:
            return Localizations.Area.Pingtung_County
        case .Hualien_County:
            return Localizations.Area.Hualien_County
        case .Taitung_County:
            return Localizations.Area.Taitung_County
        case .Penghu_County:
            return Localizations.Area.Penghu_County
        case .Kinmen_County:
            return Localizations.Area.Kinmen_County
        case .Lienchiang_County:
            return Localizations.Area.Lienchiang_County

        }
    }
    
}
