//
//  PetLoseRequest.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/08/19.
//

import Foundation

class PetLoseRequest: RequestModel {
    init(top: Int = 20, skip: Int = 20, parameter: [String: String]?){
        var queries = (["$top": top.toString, "$skip": skip.toString])
        if let parameter = parameter{
            queries.merge(parameter)
        }
        super.init(api: .petLoseList, queries: queries)
    }
}
