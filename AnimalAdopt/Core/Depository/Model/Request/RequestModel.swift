//
//  RequestModel.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/08/19.
//

import Foundation

class RequestModel{
    var request: URLRequest?

    init(api: APIs, queries: [String: String] = [:]){
        var urlComponents = URLComponents(string: APIManager.domain)
        urlComponents?.add(querys: queries)
        urlComponents?.add(querys: [api.queryKey:  api.queryValue])

        if let url = urlComponents?.url{
            request = .init(url: url)
        }
    }

}
