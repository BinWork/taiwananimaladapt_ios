//
//  AnimalRecognition.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/23.
//

import Foundation
import UIKit.UIColor

extension Animal{
    
    
    
}


//  動物認領養
struct Recognition: Decodable, Equatable{
    //  動物的流水編號
    var animal_id: Int
    //  動物的區域編號
    var animal_subid: String
    //  動物所屬縣市代碼
    var animal_area_pkid: Int
    //  動物所屬收容所代碼
    var animal_shelter_pkid: Int
    //  動物的實際所在地
    var animal_place: String
    //  動物的類型
    var animal_kind: Kind
    //  動物性別
    var animal_sex: Gender
    //  動物體型
    var animal_bodytype: BodyType
    //  動物毛色
    var animal_colour: String
    //  動物的年紀
    var animal_age: Age
    //  動物是否絕育
    var animal_sterilization: AnimalBoolean
    //  是否施打狂犬病
    var animal_bacterin: AnimalBoolean
    //  動物的尋獲地
    var animal_foundplace: String
    //  動物網頁標題
    var animal_title: String
    //  動物狀態
    var animal_status: Status
    //  資料備註
    var animal_remark: String
    /// 其他說明
    ///  （此欄位資料僅供後臺使用人員參考、紀錄，資訊不提供給一般民眾）
    private var animal_caption: String
    // 開放認養時間(起)
    var animal_opendate: String
    //  開放認養時間(迄)
    var animal_closeddate: String
    //  動物資料異動時間
    var animal_update: String
    //  動物資料建立時間
    var animal_createtime: String
    //  動物所屬收容所名稱
    var shelter_name: String
    //  圖片名稱
    var album_file: String
    //  資料更新時間
    var album_update: String
    //  資料建立時間
    var cDate: String
    //  地址
    var shelter_address: String
    //  聯絡電話
    var shelter_tel: String
    
    
    enum Kind: String, CaseIterableDefaultsLast{
        case Dog = "狗"
        case Cat = "貓"
        case Other
        
        var string: String{
            switch self{
            case .Dog:
                return Localizations.Animal.Data.Kind.dog
            case .Cat:
                return Localizations.Animal.Data.Kind.cat
            case .Other:
                return Localizations.Animal.Data.Kind.other
            }
        }
        
        var icon: UIImage?{
            switch self{
            case .Dog:
                return UIImage(named: "animalCategoryDog")
            case .Cat:
                return UIImage(named: "animalCategoryCat")
            case .Other:
                return nil
            }
        }
    }
    
    
    
    enum Gender: String, Decodable{
        case male = "M"
        case female = "F"
        case NA = "N"
        
        var string: String{
            switch self{
            case .male:
                return Localizations.Animal.Data.Gender.male
            case .female:
                return Localizations.Animal.Data.Gender.female
            case .NA:
                return Localizations.Animal.Data.Gender.unknown
            }
        }
        
        var color: UIColor{
            switch self{
            case .male:
                return UIColor.genderMale
            case .female:
                return UIColor.genderFemale
            case .NA:
                return UIColor.genderUnknown
            }
        }
        
        var icon: UIImage?{
            switch self{
            case .male:
                return UIImage(named: "male")
            case .female:
                return UIImage(named: "female")
            case .NA:
                return nil
            }
        }
        
    }
    
    enum Age: String, CaseIterableDefaultsLast{
        case child = "CHILD"
        case adult = "ADULT"
        case other
        
        var string: String{
            switch self {
            case .child:
                return Localizations.Animal.Data.Age.child
            case .adult:
                return Localizations.Animal.Data.Age.adult
            case .other:
                return Localizations.Animal.Data.Age.NA
            }
        }
    }
    
    
    enum BodyType: String, Decodable{
        case small = "SMALL"
        case medium = "MEDIUM"
        case big = "BIG"
        
        var string: String{
            switch self{
            case .small:
                return Localizations.Animal.Data.BodyType.small
            case .medium:
                return Localizations.Animal.Data.BodyType.medium
            case .big:
                return Localizations.Animal.Data.BodyType.big
            }
        }
    }
    
    enum Status: String, Decodable{
        case none = "NONE"
        case open = "OPEN"
        case adopted = "ADOPTED"
        case other = "OTHER"
        case dead = "DEAD"
        
        var string: String{
            switch self{
            case .none:
                return Localizations.Animal.Data.Status.Available.undefined
            case .open:
                return Localizations.Animal.Data.Status.Available.open
            case .adopted:
                return Localizations.Animal.Data.Status.Available.adopted
            case .other:
                return Localizations.Animal.Data.Status.Available.other
            case .dead:
                return Localizations.Animal.Data.Status.Available.dead
            }
        }
    }
    
    enum AnimalBoolean: String, Decodable{
        case yes = "T"
        case no = "F"
        case NA = "N"
        
        var string: String{
            switch self{
            case .yes:
                return Localizations.Animal.Boolean.true
            case .no:
                return Localizations.Animal.Boolean.false
            case .NA:
                return Localizations.Animal.Boolean.NA
            }
        }
    }
    
}
