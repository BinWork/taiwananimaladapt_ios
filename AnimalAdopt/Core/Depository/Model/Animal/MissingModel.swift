//
//  MissingModel.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/23.
//

import Foundation

extension Animal{
    
    // 寵物遺失
    struct MissingModel: Decodable{
        //  晶片號碼
        var chipNum: String
        //  寵物名
        var petName: String
        //  寵物別
        var petCategory: Animal.Kind
        //  性別
        var gender: Animal.Gender
        //  品種
        var variety: String
        //  毛色
        var coat: String
        //  外觀
        var exterior: String
        //  特徵
        var feature: String
        //  遺失時間
        var lostTime: String
        //  遺失地點
        var lostPlace: String
        //  飼主姓名
        var feederName: String
        //  聯絡電話
        var phoneNum: String
        //  電子郵箱
        var EMail: String
        //  照片
        var picture: String
        
        enum CodingKeys: String, CodingKey {
            case chipNum = "晶片號碼"
            case petName = "寵物名"
            case petCategory = "寵物別"
            case gender = "性別"
            case variety = "品種"
            case coat = "毛色"
            case exterior = "外觀"
            case feature = "特徵"
            case lostTime = "遺失時間"
            case lostPlace = "遺失地點"
            case feederName = "飼主姓名"
            case phoneNum = "連絡電話"
            case EMail = "EMail"
            case picture = "PICTURE"
        }
        
    }

    
}
