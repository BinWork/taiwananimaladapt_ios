//
//  AnimalRecognition.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/23.
//

import Foundation
import UIKit.UIColor

extension Animal{
    
    //  動物認領養
    struct RecognitionModel: Decodable, Equatable{
        //  動物的流水編號
        var animal_id: Int
        //  動物的區域編號
        var animal_subid: String
        //  動物所屬縣市代碼
        var animal_area_pkid: Int
        //  動物所屬收容所代碼
        var animal_shelter_pkid: Int
        //  動物的實際所在地
        var animal_place: String
        //  動物的類型
        var animal_kind: Animal.Kind
        //  動物性別
        var animal_sex: Animal.Gender
        //  動物體型
        var animal_bodytype: Animal.BodyType
        //  動物毛色
        var animal_colour: String
        //  動物的年紀
        var animal_age: Animal.Age
        //  動物是否絕育
        var animal_sterilization: Animal.Boolean
        //  是否施打狂犬病
        var animal_bacterin: Animal.Boolean
        //  動物的尋獲地
        var animal_foundplace: String
        //  動物網頁標題
        var animal_title: String
        //  動物狀態
        var animal_status: Animal.Status
        //  資料備註
        var animal_remark: String
        /// 其他說明
        ///  （此欄位資料僅供後臺使用人員參考、紀錄，資訊不提供給一般民眾）
        private var animal_caption: String
        // 開放認養時間(起)
        var animal_opendate: String
        //  開放認養時間(迄)
        var animal_closeddate: String
        //  動物資料異動時間
        var animal_update: String
        //  動物資料建立時間
        var animal_createtime: String
        //  動物所屬收容所名稱
        var shelter_name: String
        //  圖片名稱
        var album_file: String
        //  資料更新時間
        var album_update: String
        //  資料建立時間
        var cDate: String
        //  地址
        var shelter_address: String
        //  聯絡電話
        var shelter_tel: String
        
    }

    
}


