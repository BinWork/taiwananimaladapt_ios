//
//  Animal.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/23.
//

import Foundation
import UIKit.UIImage

struct Animal { }

extension Animal{
    
    enum Kind: String, Encodable, CaseIterableDefaultsLast{
        case Dog = "狗"
        case Cat = "貓"
        case Other = "其他"
        
        var string: String{
            switch self{
            case .Dog:
                return Localizations.Animal.Data.Kind.dog
            case .Cat:
                return Localizations.Animal.Data.Kind.cat
            case .Other:
                return Localizations.Animal.Data.Kind.other
            }
        }
        
        var icon: UIImage?{
            switch self{
            case .Dog:
                return UIImage(named: ImageName.Animal.Category.dog)
            case .Cat:
                return UIImage(named: ImageName.Animal.Category.cat)
            case .Other:
                return UIImage(named: ImageName.Animal.Category.other)
            }
        }
    }
    
    enum Gender: String, Encodable, CaseIterableDefaultsLast{
        case male_zh = "公"
        case male = "M"
        case female_zh = "母"
        case female = "F"
        case NA
        
        var string: String{
            switch self{
            case .male, .male_zh:
                return Localizations.Animal.Data.Gender.male
            case .female, .female_zh:
                return Localizations.Animal.Data.Gender.female
            case .NA:
                return Localizations.Animal.Data.Gender.unknown
            }
        }
        
        var color: UIColor{
            switch self{
            case .male, .male_zh:
                return UIColor.genderMale
            case .female, .female_zh:
                return UIColor.genderFemale
            case .NA:
                return UIColor.genderUnknown
            }
        }
        
        var icon: UIImage?{
            switch self{
            case .male, .male_zh:
                return UIImage(named: ImageName.Animal.Gender.male)
            case .female, .female_zh:
                return UIImage(named: ImageName.Animal.Gender.female)
            case .NA:
                return UIImage(named: ImageName.Animal.Gender.unknown)
            }
        }
    }

    enum Age: String, Encodable, CaseIterableDefaultsLast{
        case child = "CHILD"
        case adult = "ADULT"
        case other
        
        var string: String{
            switch self {
            case .child:
                return Localizations.Animal.Data.Age.child
            case .adult:
                return Localizations.Animal.Data.Age.adult
            case .other:
                return Localizations.Animal.Data.Age.NA
            }
        }
    }
    
    
    enum BodyType: String, Codable, CaseIterable{
        case small = "SMALL"
        case medium = "MEDIUM"
        case big = "BIG"
        
        var string: String{
            switch self{
            case .small:
                return Localizations.Animal.Data.BodyType.small
            case .medium:
                return Localizations.Animal.Data.BodyType.medium
            case .big:
                return Localizations.Animal.Data.BodyType.big
            }
        }
    }
    
    enum Status: String, Codable{
        case none = "NONE"
        case open = "OPEN"
        case adopted = "ADOPTED"
        case other = "OTHER"
        case dead = "DEAD"
        
        var string: String{
            switch self{
            case .none:
                return Localizations.Animal.Data.Status.Available.undefined
            case .open:
                return Localizations.Animal.Data.Status.Available.open
            case .adopted:
                return Localizations.Animal.Data.Status.Available.adopted
            case .other:
                return Localizations.Animal.Data.Status.Available.other
            case .dead:
                return Localizations.Animal.Data.Status.Available.dead
            }
        }
    }
    
    enum Boolean: String, Codable, CaseIterable{
        case yes = "T"
        case no = "F"
        case NA = "N"
        
        var string: String{
            switch self{
            case .yes:
                return Localizations.Animal.Boolean.true
            case .no:
                return Localizations.Animal.Boolean.false
            case .NA:
                return Localizations.Animal.Boolean.NA
            }
        }
    }
    
}

