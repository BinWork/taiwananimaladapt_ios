//
//  FavoriteModel.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/26.
//

import Foundation

extension Animal{
    
    struct FavoriteModel: Codable{
        
        static let key: String = "Animal.FavoriteModel"
    
        var kind: Animal.Kind
        var gender: Animal.Gender
        var statusAvailable: Animal.Status
        var sterilization: Animal.Boolean
        var bacterin: Animal.Boolean
        var bodyType: Animal.BodyType
        var age: Animal.Age
        
        var imageUrl: String
        var id: String
        var albumFile: String
        var createDate: String
        var coat: String
        var foundPlace: String
        var remark: String
        var shelterName: String
        var shelterAddress: String
        var shelterTel: String
        var openDate: String
        var lastUpdate: String
        
        static func == (lhs: Animal.FavoriteModel, rhs: Animal.FavoriteModel) -> Bool {
            return lhs.id.hash == rhs.id.hash
        }

        static func != (lhs: Animal.FavoriteModel, rhs: Animal.FavoriteModel) -> Bool {
            return lhs.id.hash != rhs.id.hash
        }
        
        init(_ viewModel: AnimalAdoptDetailViewModel){
            
            self.kind = viewModel.kind
            self.gender = viewModel.gender
            self.statusAvailable = viewModel.statusAvailable
            self.sterilization = viewModel.sterilization
            self.bacterin = viewModel.bacterin
            self.bodyType = viewModel.bodyType
            self.age = viewModel.age
            
            self.imageUrl = viewModel.imageViewModel.urlString
            self.id = viewModel.id
            self.albumFile = viewModel.albumFile
            self.createDate = viewModel.createDate
            self.coat = viewModel.coat
            self.foundPlace = viewModel.foundPlace
            self.remark = viewModel.remark
            self.shelterName = viewModel.shelterName
            self.shelterAddress = viewModel.shelterAddress
            self.openDate = viewModel.openDate
            self.shelterTel = viewModel.shelterTel
            self.lastUpdate = viewModel.lastUpdate
        }
        
    }
    
}
