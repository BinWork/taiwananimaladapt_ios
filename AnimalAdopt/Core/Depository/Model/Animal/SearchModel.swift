//
//  SearchModel.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/28.
//

import Foundation

extension Animal{
    
    //  動物認領養
    struct SearchModel: Encodable, Equatable{
        
        enum Key: String, CaseIterable{
            case animal_id = "animal_id"
            case animal_subid = "animal_subid"
            case animal_area_pkid = "animal_area_pkid"
            case animal_shelter_pkid = "animal_shelter_pkid"
            case animal_place = "animal_place"
            case animal_kind = "animal_kind"
            case animal_sex = "animal_sex"
            case animal_bodytype = "animal_bodytype"
            case animal_colour = "animal_colour"
            case animal_age = "animal_age"
            case animal_sterilization = "animal_sterilization"
            case animal_bacterin = "animal_bacterin"
            case animal_foundplace = "animal_foundplace"
            case animal_status = "animal_status"
            case animal_opendate = "animal_opendate"
            case animal_closeddate = "animal_closeddate"
            case animal_update = "animal_update"
            case animal_createtime = "animal_createtime"
            case shelter_name = "shelter_name"
            case cDate = "cDate"
            case shelter_address = "shelter_address"
        }
        
        //  動物的流水編號
        var animal_id: Int?
        //  動物的區域編號
        var animal_subid: String?
        //  動物所屬縣市代碼
        var animal_area_pkid: [AreaPkId] = []
        //  動物所屬收容所代碼
        var animal_shelter_pkid: Int?
        //  動物的實際所在地
        var animal_place: String?
        //  動物的類型
        var animal_kind: Animal.Kind?
        //  動物性別
        var animal_sex: Animal.Gender?
        //  動物體型
        var animal_bodytype: Animal.BodyType?
        //  動物毛色
        var animal_colour: String?
        //  動物的年紀
        var animal_age: Animal.Age?
        //  動物是否絕育
        var animal_sterilization: Animal.Boolean?
        //  是否施打狂犬病
        var animal_bacterin: Animal.Boolean?
        //  動物的尋獲地
        var animal_foundplace: String?
        //  動物狀態
        var animal_status: Animal.Status?
        // 開放認養時間(起)
        var animal_opendate: String?
        //  開放認養時間(迄)
        var animal_closeddate: String?
        //  動物資料異動時間
        var animal_update: String?
        //  動物資料建立時間
        var animal_createtime: String?
        //  動物所屬收容所名稱
        var shelter_name: String?
        //  資料建立時間
        var cDate: String?
        //  地址
        var shelter_address: String?
        
    }

    
}


