//
//  LocaleHandler.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/17.
//

import Foundation
import UIKit.UIFont

class LocaleHandler{
    
    private let language: Language = Language(rawValue: Locale.autoupdatingCurrent.languageCode ?? "") ?? .zh_Hans
    
    enum Language: String{
        case en_US = "en_US"
        case zh_Hans = "zh_Hans"
        
        var font: UIFont{
            switch self {
            case .en_US, .zh_Hans:
                return UIFont(name: "", size: 18) ?? .systemFont(ofSize: 18)
            default:
                return UIFont(name: "", size: 18) ?? .systemFont(ofSize: 18)
            }
        }
    }
}



