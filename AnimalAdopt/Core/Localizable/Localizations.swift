//
//  Localizations.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/23.
//

import Foundation

enum Localizations { }

extension Localizations{
    
    enum Animal{
        
        enum Data{
            static let chip = "Animal.Data.Chip".localized(comment: "The chip serial number of this animal")
            static let id = "Animal.Data.Id".localized(comment: "The id of this animal data")
            static let color = "Animal.Data.Color".localized(comment: "The animal's fur color")
            static let backterin = "Animal.Data.Backterin".localized(comment: "Whether to vaccinate against rabies")
            static let areaPkId = "Animal.Data.AreaPkId".localized(comment: "The animal's id of counties and cities")
            static let createTime = "Animal.Data.CreateTime".localized(comment: "The animal data's establish time")
            static let foundPlace = "Animal.Data.FoundPlace".localized(comment: "The place of animal been founded")
            static let openDate = "Animal.Data.OpenDate".localized(comment: "The open date of this animal data")
            static let remark = "Animal.Data.Remark".localized(comment: "The remark of this animal data")
            static let subId = "Animal.Data.SubId".localized(comment: "The subId of this animal")
            static let exterior = "Animal.Data.Exterior".localized(comment: "The exterios of this animal")
            static let feature = "Animal.Data.Feature".localized(comment: "The features of this animal")
            static let nickName = "Animal.Data.Nickname".localized(comment: "The nickname of this animal")
            static let lostDate = "Animal.Data.LostDate".localized(comment: "The date when this animal is missing from an owner")
            static let lostPlace = "Animal.Data.LostPlace".localized(comment: "The place where this animal is missing from an owner")
            
            enum Status{
                static let status = "Animal.Data.Status.Title".localized(comment: "The title of status of this animal")
                static let rabiesVaccine = "Animal.Data.Status.RabiesVaccine".localized(comment: "The status of vaccinate against rabies")
                
                enum Available{
                    static let adopted = "Animal.Data.Status.Available.Adopted".localized(comment: "The animal is adopted")
                    static let open = "Animal.Data.Status.Available.Open".localized(comment: "The animal is open")
                    static let dead = "Animal.Data.Status.Available.Dead".localized(comment: "The animal is dead")
                    static let other = "Animal.Data.Status.Available.Open".localized(comment: "The animal is in other available status")
                    static let undefined = "Animal.Data.Status.Available.Undefined".localized(comment: "The animal's status is undefined")
                }
            }
            
            
            enum Sterilization{
                static let title =  "Animal.Data.Sterilization.Title".localized(comment: "Title of sterilization")
                static let male = "Animal.Data.Sterilization.Male".localized(comment: "The sterilization for male animal")
                static let female = "Animal.Data.Sterilization.Female".localized(comment: "The sterilization for female animal")
            }
            
            enum Age{
                static let title = "Animal.Data.Age.Title".localized(comment: "The title of animal's age")
                static let child = "Animal.Data.Age.Child".localized(comment: "The animal's age category")
                static let adult = "Animal.Data.Age.Adult".localized(comment: "The animal's age category")
                static let NA = "Animal.Data.Age.NA".localized(comment: "The animal's age category")
            }
            
            enum Kind{
                static let title = "Animal.Data.Kind.Title".localized(comment: "The title of animal's kind")
                static let dog = "Animal.Data.Kind.Dog".localized(comment: "The animal's kind")
                static let cat = "Animal.Data.Kind.Cat".localized(comment: "The animal's kind")
                static let other = "Animal.Data.Kind.Other".localized(comment: "The animal's kind")
            }
            
            enum Gender{
                static let title = "Animal.Data.Gender.Title".localized(comment: "The title of animal's gender")
                static let male = "Animal.Data.Gender.Male".localized(comment: "The animal's gender")
                static let female = "Animal.Data.Gender.Female".localized(comment: "The animal's gender")
                static let unknown = "Animal.Data.Gender.Unknown".localized(comment: "The animal's gender")
            }
            
            enum BodyType{
                static let title = "Animal.Data.BodyType.Title".localized(comment: "The title of animal's body type")
                
                static let small = "Animal.Data.BodyType.Small".localized(comment: "The animal's body type")
                static let medium = "Animal.Data.BodyType.Medium".localized(comment: "The animal's body type")
                static let big = "Animal.Data.BodyType.Big".localized(comment: "The animal's body type")
            }
            
            enum Shelter{
                static let id = "Animal.Data.Shelter.Id".localized(comment: "The shelter's address where the animal belongs")
                static let name = "Animal.Data.Shelter.Name".localized(comment: "The shelter's name where the animal belongs")
                static let address = "Animal.Data.Shelter.Address".localized(comment: "The shelter's address where the animal belongs")
                static let telephone = "Animal.Data.Shelter.Telelphone".localized(comment: "The shelter's telephone number where the animal belongs")
            }
            
            enum Owner{
                static let name = "Animal.Data.Owner.Name".localized(comment: "The owner's name of this animal")
                static let email = "Animal.Data.Owner.Email".localized(comment: "The owner's email of this animal")
                static let telephone = "Animal.Data.Owner.Telephone".localized(comment: "The owner's telephone number of this animal")
            }
            
        }
        


        
        enum Boolean{
            static let `true` = "Animal.Boolean.True".localized(comment: "The animal's property with Boolean type")
            static let `false` = "Animal.Boolean.False".localized(comment: "The animal's property with Boolean type")
            static let NA = "Animal.Boolean.NA".localized(comment: "The animal's property with Boolean type")
        }
        
    }
    
    
    
    enum System{
        static let noData = "System.NoData".localized(comment: "Label of no data")
        static let tryAgain = "System.TryAgain".localized(comment: "Ask user try again later")
        
        enum Search{
            static let title = "System.Search.Title".localized(comment: "Searching Animal ViewController's title")
        }
        enum Button{
            static let reset = "System.Button.Reset".localized(comment: "Button's title of rest")
            static let confirm = "System.Button.Confirm".localized(comment: "Button's title")
        }
        
        enum Animal{
            static let imageNotFound = "System.Animal.ImageNotFound".localized(comment: "The image of animal is not found")
        }
        
    }
    

    
    
    
    enum FooterBar{
        enum Adopt{
            static let title = "FooterBar.Adopt.Title".localized(comment: "The title on tabbar item")
            static let description = "FooterBar.Adopt.Description".localized(comment: "The title on ViewController's title")
        }
        
        enum Favorite{
            static let title = "FooterBar.Favorite.Title".localized(comment: "The title on tabbar item")
            static let description = "FooterBar.Favorite.Description".localized(comment: "The title on ViewController's title")
        }
        
        enum Home{
            static let title = "FooterBar.Home.Title".localized(comment: "The title on tabbar item")
            static let description = "FooterBar.Home.Description".localized(comment: "The title on ViewController's title")
        }
        
        enum Missing{
            static let title = "FooterBar.Missing.Title".localized(comment: "The title on tabbar item")
            static let description = "FooterBar.Missing.Description".localized(comment: "The title on ViewController's title")
        }
        
    }
    
}

extension Localizations{
    
    enum Area{
        static let Taipei_City = "Area.Taipei_City".localized(comment: "Taipei City")
        static let NewTaipei_City = "Area.NewTaipei_City".localized(comment: "NewTaipei City")
        static let Keelung_City = "Area.Keelung_City".localized(comment: "Keelung City")
        static let Yilan_County = "Area.Yilan_County".localized(comment: "Yilan County")
        static let Taoyuan_City = "Area.Taoyuan_City".localized(comment: "Taoyuan City")
        static let Hsinchu_County = "Area.Hsinchu_County".localized(comment: "Hsinchu County")
        static let Hsinchu_City = "Area.Hsinchu_City".localized(comment: "Hsinchu City")
        static let Miaoli_County = "Area.Miaoli_County".localized(comment: "Miaoli County")
        static let Taichung_City = "Area.Taichung_City".localized(comment: "Taichung City")
        static let Changhua_County = "Area.Changhua_County".localized(comment: "Changhua County")
        static let Nantou_County = "Area.Nantou_County".localized(comment: "Nantou County")
        static let Yunlin_County = "Area.Yunlin_County".localized(comment: "Yunlin County")
        static let Chiayi_County = "Area.Chiayi_County".localized(comment: "Chiayi County")
        static let Chiayi_City = "Area.Chiayi_County".localized(comment: "Chiayi City")
        static let Tainan_City = "Area.Tainan_City".localized(comment: "Tainan City")
        static let Kaohsiung_City = "Area.Kaohsiung_City".localized(comment: "Kaohsiung City")
        static let Pingtung_County = "Area.Pingtung_County".localized(comment: "Pingtung County")
        static let Hualien_County = "Area.Hualien_County".localized(comment: "Hualien County")
        static let Taitung_County = "Area.Taitung_County".localized(comment: "Taitung County")
        static let Penghu_County = "Area.Penghu_County".localized(comment: "Penghu County")
        static let Kinmen_County = "Area.Kinmen_County".localized(comment: "Kinmen County")
        static let Lienchiang_County = "Area.LienChiang_County".localized(comment: "Lienchiang County")
    }
    
}
