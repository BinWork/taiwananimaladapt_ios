//
//  Ext+Dictionary.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/08/19.
//

import Foundation

extension Dictionary {
    mutating func merge(_ dict: [Key: Value]){
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }
}
