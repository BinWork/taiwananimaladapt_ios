import Foundation

extension URLComponents{
    
    mutating func add(querys: [String: String]){
        var newQueryItems: [URLQueryItem] = []
        querys.forEach { (key, value) in
            let v = value.replacingOccurrences(of: " ", with: "")
            newQueryItems.append(URLQueryItem(name: key, value: v))
        }
        if let oldQueryItems = queryItems,
           oldQueryItems.count > 0{
            queryItems = oldQueryItems + newQueryItems
        }else{
            queryItems = newQueryItems
        }
    }
    
}
