//
//  Ext+UIColor.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/13.
//

import UIKit.UIColor

extension UIColor{
    
    static let defaultTextColor: UIColor = {
        return getAdoptiveColor(
            name: "defaultTextColor", default: .black
        )
    }()
    
    static let defaultTextColor_Selected: UIColor = {
        return getAdoptiveColor(
            name: "defaultTextColor_Selected", default: .init(rgb: 0xFFFFFF)
        )
    }()
    
    static let defaultBackgroundColor: UIColor = {
        return getAdoptiveColor(
            name: "defaultBackgroundColor", default: .white
        )
    }()
    
    static let lime: UIColor = {
        return getAdoptiveColor(
            name: "lime", default: .init(rgb: 0xE5E6BA)
        )
    }()
    
    static let mustard: UIColor = {
        return getAdoptiveColor(
            name: "mustard", default: .init(rgb: 0xEBC934)
        )
    }()
    
    static let peacock: UIColor = {
        return getAdoptiveColor(
            name: "peacock", default: .init(rgb: 0x007A86)
        )
    }()
    
    static let middleGray: UIColor = {
        return getAdoptiveColor(
            name: "middleGray", default: .init(rgb: 0x007A86)
        )
    }()
    
    static let genderMale: UIColor = {
        return getAdoptiveColor(
            name: "genderMale", default: .init(rgb: 0x4A93EA)
        )
    }()
    
    static let genderFemale: UIColor = {
        return getAdoptiveColor(
            name: "genderFemale", default: .init(rgb: 0xDE73B3)
        )
    }()
    
    static let genderUnknown: UIColor = {
        return getAdoptiveColor(
            name: "genderUnknown", default: .init(rgb: 0x333333)
        )
    }()
    
    static let navigationBackgroundColor: UIColor? = {
        if #available(iOS 11.0, *) {
            return UIColor(named: "navigationBackgroundColor") ?? nil
        } else {
            return nil
        }
    }()
    
    static let footerBarBackgroundColor: UIColor? = {
        if #available(iOS 11.0, *) {
            return UIColor(named: "footerBarBackgroundColor") ?? nil
        } else {
            return nil
        }
    }()
    
    
    /// 動物的一些狀態表示 - Off
    static let animalStatus_On: UIColor? = {
        return getAdoptiveColor(
            name: "animalStatus_On", default: .init(rgb: 0x333333)
        )
    }()
    /// 動物的一些狀態表示 - Off
    static let animalStatus_Off: UIColor? = {
        return getAdoptiveColor(
            name: "animalStatus_Off", default: .init(rgb: 0x333333)
        )
    }()
    
    /// 動物的一些狀態表示 - Off
    static let animalStatusTextColor_On: UIColor? = {
        return getAdoptiveColor(
            name: "animalStatusTextColor_On", default: .init(rgb: 0x353535)
        )
    }()
    /// 動物的一些狀態表示 - Off
    static let animalStatusTextColor_Off: UIColor? = {
        return getAdoptiveColor(
            name: "animalStatusTextColor_Off", default: .init(rgb: 0xD9D9D9)
        )
    }()
    
}


extension UIColor{
    
    /// 取得包含 Dark Mode 下的 UIColor顏色
    /// - Parameters:
    ///   - colorName: 包含 dark mode 的 color set
    ///   - color: iOS 11 以下不支援 dark mode 時會回傳的顏色
    /// - Returns:
    private static func getAdoptiveColor(name colorName: String, default color: UIColor) -> UIColor{
        if #available(iOS 11.0, *) {
            return UIColor(named: colorName) ?? color
        } else {
            return color
        }
    }
    
}






extension UIColor{
    
    convenience init(red: Int, green: Int, blue: Int) {
       assert(red >= 0 && red <= 255, "Invalid red component")
       assert(green >= 0 && green <= 255, "Invalid green component")
       assert(blue >= 0 && blue <= 255, "Invalid blue component")

       self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }

    convenience init(rgb: Int) {
        self.init(red: (rgb >> 16) & 0xFF,
                  green: (rgb >> 8) & 0xFF,
                  blue: rgb & 0xFF)
        
    }
    
}
