//
//  Ext+CGFloat.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/15.
//

import Foundation
import CoreGraphics
extension CGFloat{
    
    
    /// 動物頭像的Border寬度
    static let animalImageBorderWidth: CGFloat = {
        return 3
    }()
    
    /// 預設Corner寬度
    static let defaultCornerWidth: CGFloat = {
        return 10
    }()
    
}
