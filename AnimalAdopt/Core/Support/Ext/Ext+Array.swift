//
//  Ext+Array.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/13.
//

import Foundation

extension Array where Element: Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()

        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }
        return result
    }
}
