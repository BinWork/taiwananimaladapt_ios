//
//  Ext+Int.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/05.
//

import Foundation

extension Int{
    
    var toString: String {
        return "\(self)"
    }
    
}
