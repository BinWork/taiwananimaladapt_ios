//
//  Ext+UIViewController.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/17.
//

import Foundation
import UIKit.UIViewController

extension UIViewController {

    var navigationBarHeight: CGFloat {
        if #available(iOS 13.0, *) {
            return (view.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0.0) +
                (self.navigationController?.navigationBar.frame.height ?? 0.0)
        } else {
            let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
            return topBarHeight
        }
    }
}
