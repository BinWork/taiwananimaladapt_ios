//
//  Ext+String.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/13.
//

import Foundation
import CommonCrypto

extension String{
    
    ///  如果為字串為 "" 則回傳 "無資料"
    var represent: String {
        if self.count > 0{
            return self
        }else{
            return Localizations.System.noData
        }
    }
    
    /// 本地化
    var localized: String{
        return NSLocalizedString(self, comment: "")
    }
    
    func localized(comment: String) -> String{
        return NSLocalizedString(self, comment: comment)
    }
}


extension String{
    
    var md5: String {
        let data = Data(self.utf8)
        let hash = data.withUnsafeBytes { (bytes: UnsafeRawBufferPointer) -> [UInt8] in
            var hash = [UInt8](repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
            CC_MD5(bytes.baseAddress, CC_LONG(data.count), &hash)
            return hash
        }
        return hash.map { String(format: "%02x", $0) }.joined()
    }
    
}
