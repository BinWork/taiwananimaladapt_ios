import UIKit.UIView

extension UIView{
    
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder?.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }

    func makeCorner(radius: CGFloat){
        layer.masksToBounds = true
        layer.cornerRadius = radius
    }
    func makeCornerRound(){
        layer.masksToBounds = true
        layer.cornerRadius = frame.height / 2
    }
    
    func makeBorder(width: CGFloat, color: UIColor?){
        layer.borderWidth = width
        layer.borderColor = color?.cgColor
    }
    
}

extension UIView {
    func parentView<T: UIView>(of type: T.Type) -> T? {
        guard let view = superview else {
            return nil
        }
        return (view as? T) ?? view.parentView(of: T.self)
    }
}
