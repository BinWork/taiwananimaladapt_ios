import Foundation
import SnapKit

let snpOffset = SnapKitClass()

class SnapKitClass{
    var left = 5.0
    var right = -5.0
    var top = 5.0
    var bottom = -5.0
}
