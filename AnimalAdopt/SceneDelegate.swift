//
//  SceneDelegate.swift
//  AnimalAdopt
//
//  Created by nwfmbin2 on 2021/07/05.
//

import UIKit
//import Firebase
//import FirebaseAuth

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let MyScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(windowScene: MyScene)
        guard let window = window else { return }
        window.backgroundColor = .defaultBackgroundColor
        
//        let vc = HomeViewController()
//        let navC = MainNavigationController(rootViewController: vc)
        
        let tabBarController = MainTabBarController()
        window.rootViewController = tabBarController

        // 將 UIWindow 設置為可見的
        window.makeKeyAndVisible()
        
        initFirebase()
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
        
        
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
        DataStoreManager.favorite.updateLocal()
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }


}

@available(iOS 13.0, *)
extension SceneDelegate{
    
    private func initFirebase(){
//        FirebaseApp.configure()
//        let db = Firestore.firestore()
//        track(db) // silence warning
//        signInAnonymousely()
    }
    
    private func signInAnonymousely(){
//        Auth.auth().signInAnonymously { (authData, error) in
//            if let _error = error{
//                ErrorManager.shared.handle(_error)
//                return
//            }
//            guard let _authData = authData else { return }
//
//        }
    }
    
}
